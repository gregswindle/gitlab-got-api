# gitlab-got-api

[![NPM version][npm-image]][npm-url]
[![Build Status][travis-image]][travis-url]
[![Dependency Status][daviddm-image]][daviddm-url]
[![Coverage percentage][coveralls-image]][coveralls-url]
[![standard-readme compliant][standard-readme-badge]][standard-readme-url]

> A lightweight, dynamically generated Gitlab API client based on [`got`][got-readme-url].

TODO: Fill out this long description.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [Contribute](#contribute)
- [License](#license)

## Install

```shell
npm install --save gitlab-got-api
```

## Usage

CommonJS:

```js
const { gitlab } = require('gitlab-got-api')
```

ES6+:

```js
import { gitlab } from 'gitlab-got-api'
```

Create a client:

```js
const gitlab.config()
```

## API

<var>gitlab-got-api</var> supports all _64_ Gitlab v4 APIs.

<ol>
<li><a href="award_emoji.html">Award Emoji</a></li>
<li><a href="branches.html">Branches</a></li>
<li><a href="broadcast_messages.html">Broadcast Messages</a></li>
<li><a href="project_level_variables.html">Project-level Variables</a></li>
<li><a href="group_level_variables.html">Group-level Variables</a></li>
<li><a href="snippets.html">Code Snippets</a></li>
<li><a href="commits.html">Commits</a></li>
<li><a href="custom_attributes.html">Custom Attributes</a></li>
<li><a href="deployments.html">Deployments</a></li>
<li><a href="deploy_keys.html">Deploy Keys</a></li>
<li><a href="environments.html">Environments</a></li>
<li>
<a href="epics.html">Epics</a><span class="badge-trigger ultimate"><div class="badges-drop" data-toggle="tooltip" data-placement="left auto" title="" data-original-title="Available in GitLab Ultimate and GitLab.com Gold"><span><span class="badge-small"><i class="fa fa-question-circle-o" aria-hidden="true"></i></span></span></div></span>
</li>
<li>
<a href="epic_issues.html">Epic Issues</a><span class="badge-trigger ultimate"><div class="badges-drop" data-toggle="tooltip" data-placement="left auto" title="" data-original-title="Available in GitLab Ultimate and GitLab.com Gold"><span><span class="badge-small"><i class="fa fa-question-circle-o" aria-hidden="true"></i></span></span></div></span>
</li>
<li><a href="events.html">Events</a></li>
<li><a href="features.html">Feature flags</a></li>
<li>
<a href="geo_nodes.html">Geo Nodes</a><span class="badge-trigger premium"><div class="badges-drop" data-toggle="tooltip" data-placement="left auto" title="" data-original-title="Available in GitLab Premium, GitLab.com Silver, and higher tiers"><span><span class="badge-small"><i class="fa fa-question-circle-o" aria-hidden="true"></i></span></span></div></span>
</li>
<li><a href="templates/gitignores.html">Gitignores templates</a></li>
<li><a href="templates/gitlab_ci_ymls.html">GitLab CI Config templates</a></li>
<li><a href="groups.html">Groups</a></li>
<li><a href="access_requests.html">Group Access Requests</a></li>
<li><a href="group_badges.html">Group Badges</a></li>
<li><a href="members.html">Group Members</a></li>
<li><a href="issues.html">Issues</a></li>
<li><a href="boards.html">Issue Boards</a></li>
<li><a href="issue_links.html">Issue Links</a></li>
<li><a href="group_boards.html">Group Issue Boards</a></li>
<li><a href="jobs.html">Jobs</a></li>
<li><a href="keys.html">Keys</a></li>
<li><a href="labels.html">Labels</a></li>
<li><a href="license.html">License</a></li>
<li><a href="markdown.html">Markdown</a></li>
<li><a href="merge_requests.html">Merge Requests</a></li>
<li>
<a href="merge_request_approvals.html">Merge Request Approvals</a><span class="badge-trigger starter"><div class="badges-drop" data-toggle="tooltip" data-placement="left auto" title="" data-original-title="Available in GitLab Starter, GitLab.com Bronze, and higher tiers"><span><span class="badge-small"><i class="fa fa-question-circle-o" aria-hidden="true"></i></span></span></div></span>
</li>
<li><a href="milestones.html">Project milestones</a></li>
<li><a href="group_milestones.html">Group milestones</a></li>
<li><a href="namespaces.html">Namespaces</a></li>
<li>
<a href="notes.html">Notes</a> (comments)</li>
<li>
<a href="discussions.html">Discussions</a> (threaded comments)</li>
<li><a href="notification_settings.html">Notification settings</a></li>
<li><a href="templates/licenses.html">Open source license templates</a></li>
<li><a href="pages_domains.html">Pages Domains</a></li>
<li><a href="pipelines.html">Pipelines</a></li>
<li><a href="pipeline_triggers.html">Pipeline Triggers</a></li>
<li><a href="pipeline_schedules.html">Pipeline Schedules</a></li>
<li>
<a href="projects.html">Projects</a> including setting Webhooks</li>
<li><a href="access_requests.html">Project Access Requests</a></li>
<li><a href="project_badges.html">Project Badges</a></li>
<li><a href="project_import_export.html">Project import/export</a></li>
<li><a href="members.html">Project Members</a></li>
<li><a href="project_snippets.html">Project Snippets</a></li>
<li><a href="protected_branches.html">Protected Branches</a></li>
<li><a href="repositories.html">Repositories</a></li>
<li><a href="repository_files.html">Repository Files</a></li>
<li><a href="runners.html">Runners</a></li>
<li><a href="search.html">Search</a></li>
<li><a href="services.html">Services</a></li>
<li><a href="settings.html">Settings</a></li>
<li><a href="sidekiq_metrics.html">Sidekiq metrics</a></li>
<li><a href="system_hooks.html">System Hooks</a></li>
<li><a href="tags.html">Tags</a></li>
<li><a href="todos.html">Todos</a></li>
<li><a href="users.html">Users</a></li>
<li><a href="lint.html">Validate CI configuration</a></li>
<li><a href="v3_to_v4.html">V3 to V4</a></li>
<li><a href="version.html">Version</a></li>
<li><a href="wikis.html">Wikis</a></li>
</ol>

## Maintainers

[@gregswindle][gregswindle-gl-url]

## Contribute

See [the contribute file](contribute.md)!

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

Apache-2.0 © 2018 [Greg Swndle][gregswindle-gl-url]

<!-- Badges -->

[standard-readme-badge]: https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square
[standard-readme-url]: https://github.com/RichardLitt/standard-readme
[npm-image]: https://badge.fury.io/js/gitlab-got-api.svg
[npm-url]: https://npmjs.org/package/gitlab-got-api
[travis-image]: https://travis-ci.org/gregswindle/gitlab-got-api.svg?branch=master
[travis-url]: https://travis-ci.org/gregswindle/gitlab-got-api
[daviddm-image]: https://david-dm.org/gregswindle/gitlab-got-api.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/gregswindle/gitlab-got-api
[coveralls-image]: https://coveralls.io/repos/gregswindle/gitlab-got-api/badge.svg
[coveralls-url]: https://coveralls.io/r/gregswindle/gitlab-got-api

<!-- Maintainers -->

[gregswindle-gl-url]: https://gitlab.com/gregswindle

<!-- Link and image references -->

[got-readme-url]: https://github.com/sindresorhus/got#readme
