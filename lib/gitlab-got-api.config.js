import {name, version} from '../package.json'

/**
 * Gitlab-got's got configuration.
 *
 *
 * @see https://github.com/sindresorhus/got#goturl-options
 * @see https://nodejs.org/api/https.html#https_https_request_options_callback
 * @see https://github.com/sindresorhus/got/blob/master/source/index.js#L5
 */

const defaults = {
  baseUrl: 'https://gitlab.com/api/v4',
  cache: true,
  methods: ['get', 'post', 'put', 'patch', 'head', 'delete'],
  decompress: true,
  headers: {
    'private-token': process.env.GITLAB_TOKEN,
    'user-agent': `${name}/${version} (https://github.com/sindresorhus/got)`
  },
  hooks: {
    beforeRequest: []
  },
  options: {
    retry: {
      retries: 2,
      methods: ['GET', 'PUT', 'HEAD', 'DELETE', 'OPTIONS', 'TRACE'],
      statusCodes: [408, 413, 429, 502, 503, 504]
    },
    throwHttpErrors: true,
    useElectronNet: false
  }
}

export default defaults
