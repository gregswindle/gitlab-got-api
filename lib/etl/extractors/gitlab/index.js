const gitlab = {
  accessRequests: {
    deleteGroupAccessRequestByUserId: {
      method: 'DELETE',
      operationId: 'deleteGroupAccessRequestByUserId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The user ID of the access requester',
          in: 'path',
          name: 'user_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/access_requests/:user_id',
      servers: []
    },
    getGroupAccessRequests: {
      method: 'GET',
      operationId: 'getGroupAccessRequests',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/groups/:id/access_requests',
      servers: []
    },
    postGroupAccessRequests: {
      method: 'POST',
      operationId: 'postGroupAccessRequests',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/groups/:id/access_requests',
      servers: []
    },
    putGroupAccessRequestApproves: {
      method: 'PUT',
      operationId: 'putGroupAccessRequestApproves',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The user ID of the access requester',
          in: 'path',
          name: 'user_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'A valid access level (defaults: ',
          in: 'query',
          name: 'access_level',
          required: false,
          type: ['integer']
        }
      ],
      path: '/groups/:id/access_requests/:user_id/approve',
      servers: []
    }
  },
  awardEmoji: {
    deleteProjectIssueAwardEmojiByAwardId: {
      method: 'DELETE',
      operationId: 'deleteProjectIssueAwardEmojiByAwardId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID of an issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of an award_emoji',
          in: 'path',
          name: 'award_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/award_emoji/:award_id',
      servers: []
    },
    deleteProjectIssueNoteAwardEmojiByAwardId: {
      method: 'DELETE',
      operationId: 'deleteProjectIssueNoteAwardEmojiByAwardId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID of an issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of an award_emoji',
          in: 'path',
          name: 'award_id',
          required: true,
          type: ['integer']
        }
      ],
      path:
        '/projects/:id/issues/:issue_iid/notes/:note_id/award_emoji/:award_id',
      servers: []
    },
    getProjectIssueAwardEmojiByAwardId: {
      method: 'GET',
      operationId: 'getProjectIssueAwardEmojiByAwardId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID (',
          in: 'path',
          name: 'awardable_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of the award emoji',
          in: 'path',
          name: 'award_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/award_emoji/:award_id',
      servers: []
    },
    getProjectIssueAwardEmojis: {
      method: 'GET',
      operationId: 'getProjectIssueAwardEmojis',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID (',
          in: 'path',
          name: 'awardable_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/award_emoji',
      servers: []
    },
    getProjectIssueNoteAwardEmojiByAwardId: {
      method: 'GET',
      operationId: 'getProjectIssueNoteAwardEmojiByAwardId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID of an issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of the award emoji',
          in: 'path',
          name: 'award_id',
          required: true,
          type: ['integer']
        }
      ],
      path:
        '/projects/:id/issues/:issue_iid/notes/:note_id/award_emoji/:award_id',
      servers: []
    },
    getProjectIssueNoteAwardEmojis: {
      method: 'GET',
      operationId: 'getProjectIssueNoteAwardEmojis',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID of an issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/notes/:note_id/award_emoji',
      servers: []
    },
    postProjectIssueAwardEmojis: {
      method: 'POST',
      operationId: 'postProjectIssueAwardEmojis',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID (',
          in: 'path',
          name: 'awardable_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The name of the emoji, without colons',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/award_emoji',
      servers: []
    },
    postProjectIssueNoteAwardEmojis: {
      method: 'POST',
      operationId: 'postProjectIssueNoteAwardEmojis',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID of an issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The name of the emoji, without colons',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/notes/:note_id/award_emoji',
      servers: []
    }
  },
  boards: {
    deleteProjectBoardByBoardId: {
      method: 'DELETE',
      operationId: 'deleteProjectBoardByBoardId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/boards/:board_id',
      servers: []
    },
    deleteProjectBoardListByListId: {
      method: 'DELETE',
      operationId: 'deleteProjectBoardListByListId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        },
        {
          description: "The ID of a board's list",
          in: 'path',
          name: 'list_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/boards/:board_id/lists/:list_id',
      servers: []
    },
    getProjectBoardByBoardId: {
      method: 'GET',
      operationId: 'getProjectBoardByBoardId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/boards/:board_id',
      servers: []
    },
    getProjectBoardListByListId: {
      method: 'GET',
      operationId: 'getProjectBoardListByListId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        },
        {
          description: "The ID of a board's list",
          in: 'path',
          name: 'list_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/boards/:board_id/lists/:list_id',
      servers: []
    },
    getProjectBoardLists: {
      method: 'GET',
      operationId: 'getProjectBoardLists',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/boards/:board_id/lists',
      servers: []
    },
    getProjectBoards: {
      method: 'GET',
      operationId: 'getProjectBoards',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/boards',
      servers: []
    },
    postProjectBoardLists: {
      method: 'POST',
      operationId: 'postProjectBoardLists',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a label',
          in: 'path',
          name: 'label_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/boards/:board_id/lists',
      servers: []
    },
    postProjectBoards: {
      method: 'POST',
      operationId: 'postProjectBoards',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the new board',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/boards',
      servers: []
    },
    putProjectBoardByBoardId: {
      method: 'PUT',
      operationId: 'putProjectBoardByBoardId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The new name of the board',
          in: 'query',
          name: 'name',
          required: false,
          type: ['string']
        },
        {
          description: 'The assignee the board should be scoped to',
          in: 'query',
          name: 'assignee_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'The milestone the board should be scoped to',
          in: 'query',
          name: 'milestone_id',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Comma-separated list of label names which the board should be scoped to',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description:
            'The weight range from 0 to 9, to which the board should be scoped to',
          in: 'query',
          name: 'weight',
          required: false,
          type: ['integer']
        }
      ],
      path: '/projects/:id/boards/:board_id',
      servers: []
    },
    putProjectBoardListByListId: {
      method: 'PUT',
      operationId: 'putProjectBoardListByListId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        },
        {
          description: "The ID of a board's list",
          in: 'path',
          name: 'list_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The position of the list',
          in: 'path',
          name: 'position',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/boards/:board_id/lists/:list_id',
      servers: []
    }
  },
  branches: {
    deleteProjectRepositoryBranchByBranch: {
      method: 'DELETE',
      operationId: 'deleteProjectRepositoryBranchByBranch',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the branch',
          in: 'path',
          name: 'branch',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/branches/:branch',
      servers: []
    },
    deleteProjectRepositoryMergedBranches: {
      method: 'DELETE',
      operationId: 'deleteProjectRepositoryMergedBranches',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/repository/merged_branches',
      servers: []
    },
    getProjectRepositoryBranchByBranch: {
      method: 'GET',
      operationId: 'getProjectRepositoryBranchByBranch',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the branch',
          in: 'path',
          name: 'branch',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/branches/:branch',
      servers: []
    },
    getProjectRepositoryBranches: {
      method: 'GET',
      operationId: 'getProjectRepositoryBranches',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Return list of branches matching the search criteria.',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/branches',
      servers: []
    },
    postProjectRepositoryBranches: {
      method: 'POST',
      operationId: 'postProjectRepositoryBranches',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The name of the branch',
          in: 'path',
          name: 'branch',
          required: true,
          type: ['string']
        },
        {
          description: 'The branch name or commit SHA to create branch from',
          in: 'path',
          name: 'ref',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/branches',
      servers: []
    },
    putProjectRepositoryBranchProtects: {
      method: 'PUT',
      operationId: 'putProjectRepositoryBranchProtects',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the branch',
          in: 'path',
          name: 'branch',
          required: true,
          type: ['string']
        },
        {
          description: 'Flag if developers can push to the branch',
          in: 'query',
          name: 'developers_can_push',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Flag if developers can merge to the branch',
          in: 'query',
          name: 'developers_can_merge',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/repository/branches/:branch/protect',
      servers: []
    },
    putProjectRepositoryBranchUnprotects: {
      method: 'PUT',
      operationId: 'putProjectRepositoryBranchUnprotects',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the branch',
          in: 'path',
          name: 'branch',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/branches/:branch/unprotect',
      servers: []
    }
  },
  broadcastMessages: {
    deleteBroadcastMessageById: {
      method: 'DELETE',
      operationId: 'deleteBroadcastMessageById',
      parameters: [
        {
          description: 'Broadcast message ID',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/broadcast_messages/:id',
      servers: []
    },
    getBroadcastMessageById: {
      method: 'GET',
      operationId: 'getBroadcastMessageById',
      parameters: [
        {
          description: 'Broadcast message ID',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/broadcast_messages/:id',
      servers: []
    },
    postBroadcastMessages: {
      method: 'POST',
      operationId: 'postBroadcastMessages',
      parameters: [
        {
          description: 'Message to display',
          in: 'path',
          name: 'message',
          required: true,
          type: ['string']
        },
        {
          description: 'Starting time (defaults to current time)',
          in: 'query',
          name: 'starts_at',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Ending time (defaults to one hour from current time)',
          in: 'query',
          name: 'ends_at',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Background color hex code',
          in: 'query',
          name: 'color',
          required: false,
          type: ['string']
        },
        {
          description: 'Foreground color hex code',
          in: 'query',
          name: 'font',
          required: false,
          type: ['string']
        }
      ],
      path: '/broadcast_messages',
      servers: []
    },
    putBroadcastMessageById: {
      method: 'PUT',
      operationId: 'putBroadcastMessageById',
      parameters: [
        {
          description: 'Broadcast message ID',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'Message to display',
          in: 'query',
          name: 'message',
          required: false,
          type: ['string']
        },
        {
          description: 'Starting time',
          in: 'query',
          name: 'starts_at',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Ending time',
          in: 'query',
          name: 'ends_at',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Background color hex code',
          in: 'query',
          name: 'color',
          required: false,
          type: ['string']
        },
        {
          description: 'Foreground color hex code',
          in: 'query',
          name: 'font',
          required: false,
          type: ['string']
        }
      ],
      path: '/broadcast_messages/:id',
      servers: []
    }
  },
  commits: {
    getProjectRepositoryCommitBySha: {
      method: 'GET',
      operationId: 'getProjectRepositoryCommitBySha',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The commit hash or name of a repository branch or tag',
          in: 'path',
          name: 'sha',
          required: true,
          type: ['string']
        },
        {
          description: 'Include commit stats. Default is true',
          in: 'query',
          name: 'stats',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/repository/commits/:sha',
      servers: []
    },
    getProjectRepositoryCommitComments: {
      method: 'GET',
      operationId: 'getProjectRepositoryCommitComments',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The commit hash or name of a repository branch or tag',
          in: 'path',
          name: 'sha',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/commits/:sha/comments',
      servers: []
    },
    getProjectRepositoryCommitDiffs: {
      method: 'GET',
      operationId: 'getProjectRepositoryCommitDiffs',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The commit hash or name of a repository branch or tag',
          in: 'path',
          name: 'sha',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/commits/:sha/diff',
      servers: []
    },
    getProjectRepositoryCommitMergeRequests: {
      method: 'GET',
      operationId: 'getProjectRepositoryCommitMergeRequests',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The commit SHA',
          in: 'path',
          name: 'sha',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/commits/:sha/merge_requests',
      servers: []
    },
    getProjectRepositoryCommitRefs: {
      method: 'GET',
      operationId: 'getProjectRepositoryCommitRefs',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The commit hash',
          in: 'path',
          name: 'sha',
          required: true,
          type: ['string']
        },
        {
          description: 'The scope of commits. Possible values ',
          in: 'query',
          name: 'type',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/commits/:sha/refs',
      servers: []
    },
    getProjectRepositoryCommitStatuses: {
      method: 'GET',
      operationId: 'getProjectRepositoryCommitStatuses',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The commit SHA',
          in: 'path',
          name: 'sha',
          required: true,
          type: ['string']
        },
        {
          description:
            'The name of a repository branch or tag or, if not given, the default branch',
          in: 'query',
          name: 'ref',
          required: false,
          type: ['string']
        },
        {
          description: 'Filter by ',
          in: 'query',
          name: 'stage',
          required: false,
          type: ['string']
        },
        {
          description: 'Filter by ',
          in: 'query',
          name: 'name',
          required: false,
          type: ['string']
        },
        {
          description: 'Return all statuses, not only the latest ones',
          in: 'query',
          name: 'all',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/repository/commits/:sha/statuses',
      servers: []
    },
    getProjectRepositoryCommits: {
      method: 'GET',
      operationId: 'getProjectRepositoryCommits',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description:
            'The name of a repository branch or tag or if not given the default branch',
          in: 'query',
          name: 'ref_name',
          required: false,
          type: ['string']
        },
        {
          description:
            'Only commits after or on this date will be returned in ISO 8601 format YYYY-MM-DDTHH:MM:SSZ',
          in: 'query',
          name: 'since',
          required: false,
          type: ['string']
        },
        {
          description:
            'Only commits before or on this date will be returned in ISO 8601 format YYYY-MM-DDTHH:MM:SSZ',
          in: 'query',
          name: 'until',
          required: false,
          type: ['string']
        },
        {
          description: 'The file path',
          in: 'query',
          name: 'path',
          required: false,
          type: ['string']
        },
        {
          description: 'Retrieve every commit from the repository',
          in: 'query',
          name: 'all',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Stats about each commit will be added to the response',
          in: 'query',
          name: 'with_stats',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/repository/commits',
      servers: []
    },
    postProjectRepositoryCommitCherryPicks: {
      method: 'POST',
      operationId: 'postProjectRepositoryCommitCherryPicks',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The commit hash',
          in: 'path',
          name: 'sha',
          required: true,
          type: ['string']
        },
        {
          description: 'The name of the branch',
          in: 'path',
          name: 'branch',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/commits/:sha/cherry_pick',
      servers: []
    },
    postProjectRepositoryCommitComments: {
      method: 'POST',
      operationId: 'postProjectRepositoryCommitComments',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The commit SHA or name of a repository branch or tag',
          in: 'path',
          name: 'sha',
          required: true,
          type: ['string']
        },
        {
          description: 'The text of the comment',
          in: 'path',
          name: 'note',
          required: true,
          type: ['string']
        },
        {
          description: 'The file path relative to the repository',
          in: 'query',
          name: 'path',
          required: false,
          type: ['string']
        },
        {
          description: 'The line number where the comment should be placed',
          in: 'query',
          name: 'line',
          required: false,
          type: ['integer']
        },
        {
          description: 'The line type. Takes ',
          in: 'query',
          name: 'line_type',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/commits/:sha/comments',
      servers: []
    },
    postProjectRepositoryCommits: {
      method: 'POST',
      operationId: 'postProjectRepositoryCommits',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description:
            'Name of the branch to commit into. To create a new branch, also provide ',
          in: 'path',
          name: 'branch',
          required: true,
          type: ['string']
        },
        {
          description: 'Commit message',
          in: 'path',
          name: 'commit_message',
          required: true,
          type: ['string']
        },
        {
          description: 'Name of the branch to start the new commit from',
          in: 'query',
          name: 'start_branch',
          required: false,
          type: ['string']
        },
        {
          description:
            'An array of action hashes to commit as a batch. See the next table for what attributes it can take.',
          in: 'path',
          name: 'actions[]',
          required: true,
          type: ['array']
        },
        {
          description: "Specify the commit author's email address",
          in: 'query',
          name: 'author_email',
          required: false,
          type: ['string']
        },
        {
          description: "Specify the commit author's name",
          in: 'query',
          name: 'author_name',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/commits',
      servers: []
    },
    postProjectStatusBySha: {
      method: 'POST',
      operationId: 'postProjectStatusBySha',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The commit SHA',
          in: 'path',
          name: 'sha',
          required: true,
          type: ['string']
        },
        {
          description: 'The state of the status. Can be one of the following: ',
          in: 'path',
          name: 'state',
          required: true,
          type: ['string']
        },
        {
          description: 'The ',
          in: 'query',
          name: 'ref',
          required: false,
          type: ['string']
        },
        {
          description:
            'The label to differentiate this status from the status of other systems. Default value is ',
          in: 'query',
          name: 'name',
          required: false,
          type: ['string']
        },
        {
          description: 'The target URL to associate with this status',
          in: 'query',
          name: 'target_url',
          required: false,
          type: ['string']
        },
        {
          description: 'The short description of the status',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: 'The total code coverage',
          in: 'query',
          name: 'coverage',
          required: false,
          type: ['float']
        }
      ],
      path: '/projects/:id/statuses/:sha',
      servers: []
    }
  },
  customAttributes: {
    deleteUserCustomAttributeByKey: {
      method: 'DELETE',
      operationId: 'deleteUserCustomAttributeByKey',
      parameters: [
        {
          description: 'The ID of a resource',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The key of the custom attribute',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        }
      ],
      path: '/users/:id/custom_attributes/:key',
      servers: []
    },
    getUserCustomAttributeByKey: {
      method: 'GET',
      operationId: 'getUserCustomAttributeByKey',
      parameters: [
        {
          description: 'The ID of a resource',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The key of the custom attribute',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        }
      ],
      path: '/users/:id/custom_attributes/:key',
      servers: []
    },
    getUserCustomAttributes: {
      method: 'GET',
      operationId: 'getUserCustomAttributes',
      parameters: [
        {
          description: 'The ID of a resource',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/users/:id/custom_attributes',
      servers: []
    },
    putUserCustomAttributeByKey: {
      method: 'PUT',
      operationId: 'putUserCustomAttributeByKey',
      parameters: [
        {
          description: 'The ID of a resource',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The key of the custom attribute',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        },
        {
          description: 'The value of the custom attribute',
          in: 'path',
          name: 'value',
          required: true,
          type: ['string']
        }
      ],
      path: '/users/:id/custom_attributes/:key',
      servers: []
    }
  },
  deployKeys: {
    deleteProjectDeployKeyByKeyId: {
      method: 'DELETE',
      operationId: 'deleteProjectDeployKeyByKeyId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of the deploy key',
          in: 'path',
          name: 'key_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/deploy_keys/:key_id',
      servers: []
    },
    getProjectDeployKeyByKeyId: {
      method: 'GET',
      operationId: 'getProjectDeployKeyByKeyId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of the deploy key',
          in: 'path',
          name: 'key_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/deploy_keys/:key_id',
      servers: []
    },
    getProjectDeployKeys: {
      method: 'GET',
      operationId: 'getProjectDeployKeys',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/deploy_keys',
      servers: []
    },
    postProjectDeployKeys: {
      method: 'POST',
      operationId: 'postProjectDeployKeys',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "New deploy key's title",
          in: 'path',
          name: 'title',
          required: true,
          type: ['string']
        },
        {
          description: 'New deploy key',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        },
        {
          description: "Can deploy key push to the project's repository",
          in: 'query',
          name: 'can_push',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/deploy_keys',
      servers: []
    },
    putProjectDeployKeyByKeyId: {
      method: 'PUT',
      operationId: 'putProjectDeployKeyByKeyId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "New deploy key's title",
          in: 'query',
          name: 'title',
          required: false,
          type: ['string']
        },
        {
          description: "Can deploy key push to the project's repository",
          in: 'query',
          name: 'can_push',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/deploy_keys/:key_id',
      servers: []
    }
  },
  deployments: {
    getProjectDeploymentByDeploymentId: {
      method: 'GET',
      operationId: 'getProjectDeploymentByDeploymentId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of the deployment',
          in: 'path',
          name: 'deployment_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/deployments/:deployment_id',
      servers: []
    },
    getProjectDeployments: {
      method: 'GET',
      operationId: 'getProjectDeployments',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Return deployments ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return deployments sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/deployments',
      servers: []
    }
  },
  discussions: {
    deleteGroupEpicDiscussionNoteByNoteId: {
      method: 'DELETE',
      operationId: 'deleteGroupEpicDiscussionNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an epic',
          in: 'path',
          name: 'epic_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        }
      ],
      path:
        '/groups/:id/epics/:epic_id/discussions/:discussion_id/notes/:note_id',
      servers: []
    },
    deleteProjectCommitDiscussionNoteByNoteId: {
      method: 'DELETE',
      operationId: 'deleteProjectCommitDiscussionNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a commit',
          in: 'path',
          name: 'commit_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        }
      ],
      path:
        '/projects/:id/commits/:commit_id/discussions/:discussion_id/notes/:note_id',
      servers: []
    },
    deleteProjectIssueDiscussionNoteByNoteId: {
      method: 'DELETE',
      operationId: 'deleteProjectIssueDiscussionNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of an issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        }
      ],
      path:
        '/projects/:id/issues/:issue_iid/discussions/:discussion_id/notes/:note_id',
      servers: []
    },
    deleteProjectMergeRequestDiscussionNoteByNoteId: {
      method: 'DELETE',
      operationId: 'deleteProjectMergeRequestDiscussionNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of a merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        }
      ],
      path:
        '/projects/:id/merge_requests/:merge_request_iid/discussions/:discussion_id/notes/:note_id',
      servers: []
    },
    deleteProjectSnippetDiscussionNoteByNoteId: {
      method: 'DELETE',
      operationId: 'deleteProjectSnippetDiscussionNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an snippet',
          in: 'path',
          name: 'snippet_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        }
      ],
      path:
        '/projects/:id/snippets/:snippet_id/discussions/:discussion_id/notes/:note_id',
      servers: []
    },
    getGroupEpicDiscussionByDiscussionId: {
      method: 'GET',
      operationId: 'getGroupEpicDiscussionByDiscussionId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an epic',
          in: 'path',
          name: 'epic_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/epics/:epic_id/discussions/:discussion_id',
      servers: []
    },
    getGroupEpicDiscussions: {
      method: 'GET',
      operationId: 'getGroupEpicDiscussions',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an epic',
          in: 'path',
          name: 'epic_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/epics/:epic_id/discussions',
      servers: []
    },
    getProjectCommitDiscussionByDiscussionId: {
      method: 'GET',
      operationId: 'getProjectCommitDiscussionByDiscussionId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a commit',
          in: 'path',
          name: 'commit_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/commits/:commit_id/discussions/:discussion_id',
      servers: []
    },
    getProjectCommitDiscussions: {
      method: 'GET',
      operationId: 'getProjectCommitDiscussions',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a commit',
          in: 'path',
          name: 'commit_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/commits/:commit_id/discussions',
      servers: []
    },
    getProjectIssueDiscussionByDiscussionId: {
      method: 'GET',
      operationId: 'getProjectIssueDiscussionByDiscussionId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of an issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/discussions/:discussion_id',
      servers: []
    },
    getProjectIssueDiscussions: {
      method: 'GET',
      operationId: 'getProjectIssueDiscussions',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of an issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/discussions',
      servers: []
    },
    getProjectMergeRequestDiscussionByDiscussionId: {
      method: 'GET',
      operationId: 'getProjectMergeRequestDiscussionByDiscussionId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of a merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        }
      ],
      path:
        '/projects/:id/merge_requests/:merge_request_iid/discussions/:discussion_id',
      servers: []
    },
    getProjectMergeRequestDiscussions: {
      method: 'GET',
      operationId: 'getProjectMergeRequestDiscussions',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of a merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/discussions',
      servers: []
    },
    getProjectSnippetDiscussionByDiscussionId: {
      method: 'GET',
      operationId: 'getProjectSnippetDiscussionByDiscussionId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an snippet',
          in: 'path',
          name: 'snippet_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/snippets/:snippet_id/discussions/:discussion_id',
      servers: []
    },
    getProjectSnippetDiscussions: {
      method: 'GET',
      operationId: 'getProjectSnippetDiscussions',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an snippet',
          in: 'path',
          name: 'snippet_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/snippets/:snippet_id/discussions',
      servers: []
    },
    postGroupEpicDiscussionNotes: {
      method: 'POST',
      operationId: 'postGroupEpicDiscussionNotes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an epic',
          in: 'path',
          name: 'epic_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        },
        {
          description:
            'Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z',
          in: 'query',
          name: 'created_at',
          required: false,
          type: ['string']
        }
      ],
      path: '/groups/:id/epics/:epic_id/discussions/:discussion_id/notes',
      servers: []
    },
    postGroupEpicDiscussions: {
      method: 'POST',
      operationId: 'postGroupEpicDiscussions',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an epic',
          in: 'path',
          name: 'epic_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        },
        {
          description:
            'Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z',
          in: 'query',
          name: 'created_at',
          required: false,
          type: ['string']
        }
      ],
      path: '/groups/:id/epics/:epic_id/discussions',
      servers: []
    },
    postProjectCommitDiscussionNotes: {
      method: 'POST',
      operationId: 'postProjectCommitDiscussionNotes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a commit',
          in: 'path',
          name: 'commit_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        },
        {
          description:
            'Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z',
          in: 'query',
          name: 'created_at',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/commits/:commit_id/discussions/:discussion_id/notes',
      servers: []
    },
    postProjectCommitDiscussions: {
      method: 'POST',
      operationId: 'postProjectCommitDiscussions',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a commit',
          in: 'path',
          name: 'commit_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        },
        {
          description:
            'Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z',
          in: 'query',
          name: 'created_at',
          required: false,
          type: ['string']
        },
        {
          description: 'Position when creating a diff note',
          in: 'query',
          name: 'position',
          required: false,
          type: ['hash']
        },
        {
          description: 'Base commit SHA in the source branch',
          in: 'path',
          name: 'position[base_sha]',
          required: true,
          type: ['string']
        },
        {
          description: 'SHA referencing commit in target branch',
          in: 'path',
          name: 'position[start_sha]',
          required: true,
          type: ['string']
        },
        {
          description: 'SHA referencing HEAD of this commit',
          in: 'path',
          name: 'position[head_sha]',
          required: true,
          type: ['string']
        },
        {
          description:
            "Type of the position reference', allowed values: 'text' or 'image'",
          in: 'path',
          name: 'position[position_type]',
          required: true,
          type: ['string']
        },
        {
          description: 'File path after change',
          in: 'query',
          name: 'position[new_path]',
          required: false,
          type: ['string']
        },
        {
          description: 'Line number after change',
          in: 'query',
          name: 'position[new_line]',
          required: false,
          type: ['integer']
        },
        {
          description: 'File path before change',
          in: 'query',
          name: 'position[old_path]',
          required: false,
          type: ['string']
        },
        {
          description: 'Line number before change',
          in: 'query',
          name: 'position[old_line]',
          required: false,
          type: ['integer']
        },
        {
          description: "Width of the image (for 'image' diff notes)",
          in: 'query',
          name: 'position[width]',
          required: false,
          type: ['integer']
        },
        {
          description: "Height of the image (for 'image' diff notes)",
          in: 'query',
          name: 'position[height]',
          required: false,
          type: ['integer']
        },
        {
          description: "X coordinate (for 'image' diff notes)",
          in: 'query',
          name: 'position[x]',
          required: false,
          type: ['integer']
        },
        {
          description: "Y coordinate (for 'image' diff notes)",
          in: 'query',
          name: 'position[y]',
          required: false,
          type: ['integer']
        }
      ],
      path: '/projects/:id/commits/:commit_id/discussions',
      servers: []
    },
    postProjectIssueDiscussionNotes: {
      method: 'POST',
      operationId: 'postProjectIssueDiscussionNotes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of an issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        },
        {
          description:
            'Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z',
          in: 'query',
          name: 'created_at',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/discussions/:discussion_id/notes',
      servers: []
    },
    postProjectIssueDiscussions: {
      method: 'POST',
      operationId: 'postProjectIssueDiscussions',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of an issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        },
        {
          description:
            'Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z',
          in: 'query',
          name: 'created_at',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/discussions',
      servers: []
    },
    postProjectMergeRequestDiscussionNotes: {
      method: 'POST',
      operationId: 'postProjectMergeRequestDiscussionNotes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of a merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        },
        {
          description:
            'Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z',
          in: 'query',
          name: 'created_at',
          required: false,
          type: ['string']
        }
      ],
      path:
        '/projects/:id/merge_requests/:merge_request_iid/discussions/:discussion_id/notes',
      servers: []
    },
    postProjectMergeRequestDiscussions: {
      method: 'POST',
      operationId: 'postProjectMergeRequestDiscussions',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of a merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        },
        {
          description:
            'Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z',
          in: 'query',
          name: 'created_at',
          required: false,
          type: ['string']
        },
        {
          description: 'Position when creating a diff note',
          in: 'query',
          name: 'position',
          required: false,
          type: ['hash']
        },
        {
          description: 'Base commit SHA in the source branch',
          in: 'path',
          name: 'position[base_sha]',
          required: true,
          type: ['string']
        },
        {
          description: 'SHA referencing commit in target branch',
          in: 'path',
          name: 'position[start_sha]',
          required: true,
          type: ['string']
        },
        {
          description: 'SHA referencing HEAD of this merge request',
          in: 'path',
          name: 'position[head_sha]',
          required: true,
          type: ['string']
        },
        {
          description:
            "Type of the position reference', allowed values: 'text' or 'image'",
          in: 'path',
          name: 'position[position_type]',
          required: true,
          type: ['string']
        },
        {
          description: 'File path after change',
          in: 'query',
          name: 'position[new_path]',
          required: false,
          type: ['string']
        },
        {
          description: "Line number after change (for 'text' diff notes)",
          in: 'query',
          name: 'position[new_line]',
          required: false,
          type: ['integer']
        },
        {
          description: 'File path before change',
          in: 'query',
          name: 'position[old_path]',
          required: false,
          type: ['string']
        },
        {
          description: "Line number before change (for 'text' diff notes)",
          in: 'query',
          name: 'position[old_line]',
          required: false,
          type: ['integer']
        },
        {
          description: "Width of the image (for 'image' diff notes)",
          in: 'query',
          name: 'position[width]',
          required: false,
          type: ['integer']
        },
        {
          description: "Height of the image (for 'image' diff notes)",
          in: 'query',
          name: 'position[height]',
          required: false,
          type: ['integer']
        },
        {
          description: "X coordinate (for 'image' diff notes)",
          in: 'query',
          name: 'position[x]',
          required: false,
          type: ['integer']
        },
        {
          description: "Y coordinate (for 'image' diff notes)",
          in: 'query',
          name: 'position[y]',
          required: false,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/discussions',
      servers: []
    },
    postProjectSnippetDiscussionNotes: {
      method: 'POST',
      operationId: 'postProjectSnippetDiscussionNotes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an snippet',
          in: 'path',
          name: 'snippet_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        },
        {
          description:
            'Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z',
          in: 'query',
          name: 'created_at',
          required: false,
          type: ['string']
        }
      ],
      path:
        '/projects/:id/snippets/:snippet_id/discussions/:discussion_id/notes',
      servers: []
    },
    postProjectSnippetDiscussions: {
      method: 'POST',
      operationId: 'postProjectSnippetDiscussions',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an snippet',
          in: 'path',
          name: 'snippet_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        },
        {
          description:
            'Date time string, ISO 8601 formatted, e.g. 2016-03-11T03:45:40Z',
          in: 'query',
          name: 'created_at',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/snippets/:snippet_id/discussions',
      servers: []
    },
    putGroupEpicDiscussionNoteByNoteId: {
      method: 'PUT',
      operationId: 'putGroupEpicDiscussionNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an epic',
          in: 'path',
          name: 'epic_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        }
      ],
      path:
        '/groups/:id/epics/:epic_id/discussions/:discussion_id/notes/:note_id',
      servers: []
    },
    putProjectCommitDiscussionNoteByNoteId: {
      method: 'PUT',
      operationId: 'putProjectCommitDiscussionNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a commit',
          in: 'path',
          name: 'commit_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a note',
          in: 'query',
          name: 'body',
          required: false,
          type: ['string']
        }
      ],
      path:
        '/projects/:id/commits/:commit_id/discussions/:discussion_id/notes/:note_id',
      servers: []
    },
    putProjectIssueDiscussionNoteByNoteId: {
      method: 'PUT',
      operationId: 'putProjectIssueDiscussionNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of an issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        }
      ],
      path:
        '/projects/:id/issues/:issue_iid/discussions/:discussion_id/notes/:note_id',
      servers: []
    },
    putProjectMergeRequestDiscussionByDiscussionId: {
      method: 'PUT',
      operationId: 'putProjectMergeRequestDiscussionByDiscussionId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of a merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'Resolve/unresolve the discussion',
          in: 'path',
          name: 'resolved',
          required: true,
          type: ['boolean']
        }
      ],
      path:
        '/projects/:id/merge_requests/:merge_request_iid/discussions/:discussion_id',
      servers: []
    },
    putProjectMergeRequestDiscussionNoteByNoteId: {
      method: 'PUT',
      operationId: 'putProjectMergeRequestDiscussionNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of a merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion (exactly one of ',
          in: 'query',
          name: 'body',
          required: false,
          type: ['string']
        },
        {
          description: 'Resolve/unresolve the note (exactly one of ',
          in: 'query',
          name: 'resolved',
          required: false,
          type: ['boolean']
        }
      ],
      path:
        '/projects/:id/merge_requests/:merge_request_iid/discussions/:discussion_id/notes/:note_id',
      servers: []
    },
    putProjectSnippetDiscussionNoteByNoteId: {
      method: 'PUT',
      operationId: 'putProjectSnippetDiscussionNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an snippet',
          in: 'path',
          name: 'snippet_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion',
          in: 'path',
          name: 'discussion_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a discussion note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a discussion',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        }
      ],
      path:
        '/projects/:id/snippets/:snippet_id/discussions/:discussion_id/notes/:note_id',
      servers: []
    }
  },
  environments: {
    deleteProjectEnvironmentByEnvironmentId: {
      method: 'DELETE',
      operationId: 'deleteProjectEnvironmentByEnvironmentId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of the environment',
          in: 'path',
          name: 'environment_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/environments/:environment_id',
      servers: []
    },
    getProjectEnvironments: {
      method: 'GET',
      operationId: 'getProjectEnvironments',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/environments',
      servers: []
    },
    postProjectEnvironmentStops: {
      method: 'POST',
      operationId: 'postProjectEnvironmentStops',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of the environment',
          in: 'path',
          name: 'environment_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/environments/:environment_id/stop',
      servers: []
    },
    postProjectEnvironments: {
      method: 'POST',
      operationId: 'postProjectEnvironments',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the environment',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        },
        {
          description: 'Place to link to for this environment',
          in: 'query',
          name: 'external_url',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/environments',
      servers: []
    },
    putProjectEnvironmentByEnvironmentsId: {
      method: 'PUT',
      operationId: 'putProjectEnvironmentByEnvironmentsId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of the environment',
          in: 'path',
          name: 'environment_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The new name of the environment',
          in: 'query',
          name: 'name',
          required: false,
          type: ['string']
        },
        {
          description: 'The new external_url',
          in: 'query',
          name: 'external_url',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/environments/:environments_id',
      servers: []
    }
  },
  epicIssues: {
    deleteGroupEpicIssueByEpicIssueId: {
      method: 'DELETE',
      operationId: 'deleteGroupEpicIssueByEpicIssueId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID  of the epic.',
          in: 'path',
          name: 'epic_iid',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID  of the issue - epic association.',
          in: 'path',
          name: 'epic_issue_id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/groups/:id/epics/:epic_iid/issues/:epic_issue_id',
      servers: []
    },
    getGroupEpicIssues: {
      method: 'GET',
      operationId: 'getGroupEpicIssues',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID  of the epic.',
          in: 'path',
          name: 'epic_iid',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/groups/:id/epics/:epic_iid/issues',
      servers: []
    },
    postGroupEpicIssueByIssueId: {
      method: 'POST',
      operationId: 'postGroupEpicIssueByIssueId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID  of the epic.',
          in: 'path',
          name: 'epic_iid',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID  of the issue.',
          in: 'path',
          name: 'issue_id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/groups/:id/epics/:epic_iid/issues/:issue_id',
      servers: []
    },
    putGroupEpicIssueByEpicIssueId: {
      method: 'PUT',
      operationId: 'putGroupEpicIssueByEpicIssueId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID  of the epic.',
          in: 'path',
          name: 'epic_iid',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of the issue - epic association.',
          in: 'path',
          name: 'epic_issue_id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description:
            'The ID of the issue - epic association that should be placed before the link in the question.',
          in: 'query',
          name: 'move_before_id',
          required: false,
          type: ['integer', 'string']
        },
        {
          description:
            'The ID of the issue - epic association that should be placed after the link in the question.',
          in: 'query',
          name: 'move_after_id',
          required: false,
          type: ['integer', 'string']
        }
      ],
      path: '/groups/:id/epics/:epic_iid/issues/:epic_issue_id',
      servers: []
    }
  },
  epics: {
    deleteGroupEpicByEpicIid: {
      method: 'DELETE',
      operationId: 'deleteGroupEpicByEpicIid',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID  of the epic.',
          in: 'path',
          name: 'epic_iid',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/groups/:id/epics/:epic_iid',
      servers: []
    },
    getGroupEpicByEpicIid: {
      method: 'GET',
      operationId: 'getGroupEpicByEpicIid',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID  of the epic.',
          in: 'path',
          name: 'epic_iid',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/groups/:id/epics/:epic_iid',
      servers: []
    },
    getGroupEpics: {
      method: 'GET',
      operationId: 'getGroupEpics',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Return epics created by the given user ',
          in: 'query',
          name: 'author_id',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Return epics matching a comma separated list of labels names. Label names from the epic group or a parent group can be used',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description: 'Return epics ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return epics sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description: 'Search epics against their ',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        }
      ],
      path: '/groups/:id/epics',
      servers: []
    },
    postGroupEpics: {
      method: 'POST',
      operationId: 'postGroupEpics',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The title of the epic',
          in: 'path',
          name: 'title',
          required: true,
          type: ['string']
        },
        {
          description: 'The comma separated list of labels',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description: 'The description of the epic',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: 'The start date of the epic',
          in: 'query',
          name: 'start_date',
          required: false,
          type: ['string']
        },
        {
          description: 'The end date of the epic',
          in: 'query',
          name: 'end_date',
          required: false,
          type: ['string.']
        }
      ],
      path: '/groups/:id/epics',
      servers: []
    },
    putGroupEpicByEpicIid: {
      method: 'PUT',
      operationId: 'putGroupEpicByEpicIid',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID  of the epic',
          in: 'path',
          name: 'epic_iid',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The title of an epic',
          in: 'query',
          name: 'title',
          required: false,
          type: ['string']
        },
        {
          description: 'The description of an epic',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: 'The comma separated list of labels',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description: 'The start date of an epic',
          in: 'query',
          name: 'start_date',
          required: false,
          type: ['string']
        },
        {
          description: 'The end date of an epic',
          in: 'query',
          name: 'end_date',
          required: false,
          type: ['string.']
        }
      ],
      path: '/groups/:id/epics/:epic_iid',
      servers: []
    }
  },
  events: {
    getEvents: {
      method: 'GET',
      operationId: 'getEvents',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'project_id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Include only events of a particular ',
          in: 'query',
          name: 'action',
          required: false,
          type: ['string']
        },
        {
          description: 'Include only events of a particular ',
          in: 'query',
          name: 'target_type',
          required: false,
          type: ['string']
        },
        {
          description:
            'Include only events created before a particular date. Please see ',
          in: 'query',
          name: 'before',
          required: false,
          type: ['date']
        },
        {
          description:
            'Include only events created after a particular date. Please see ',
          in: 'query',
          name: 'after',
          required: false,
          type: ['date']
        },
        {
          description: 'Sort events in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        }
      ],
      path: '/:project_id/events',
      servers: []
    },
    getUserEvents: {
      method: 'GET',
      operationId: 'getUserEvents',
      parameters: [
        {
          description: 'The ID or Username of the user',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'Include only events of a particular ',
          in: 'query',
          name: 'action',
          required: false,
          type: ['string']
        },
        {
          description: 'Include only events of a particular ',
          in: 'query',
          name: 'target_type',
          required: false,
          type: ['string']
        },
        {
          description:
            'Include only events created before a particular date. Please see ',
          in: 'query',
          name: 'before',
          required: false,
          type: ['date']
        },
        {
          description:
            'Include only events created after a particular date. Please see ',
          in: 'query',
          name: 'after',
          required: false,
          type: ['date']
        },
        {
          description: 'Sort events in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        }
      ],
      path: '/users/:id/events',
      servers: []
    }
  },
  features: {
    postFeatureByName: {
      method: 'POST',
      operationId: 'postFeatureByName',
      parameters: [
        {
          description: 'Name of the feature to create or update',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        },
        {
          description: 'true',
          in: 'path',
          name: 'value',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'A Feature group name',
          in: 'query',
          name: 'feature_group',
          required: false,
          type: ['string']
        },
        {
          description: 'A GitLab username',
          in: 'query',
          name: 'user',
          required: false,
          type: ['string']
        }
      ],
      path: '/features/:name',
      servers: []
    }
  },
  geoNodes: {
    deleteGeoNodeById: {
      method: 'DELETE',
      operationId: 'deleteGeoNodeById',
      parameters: [
        {
          description: 'The ID of the Geo node.',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/geo_nodes/:id',
      servers: []
    },
    getGeoNodesCurrentFailures: {
      method: 'GET',
      operationId: 'getGeoNodesCurrentFailures',
      parameters: [
        {
          description: 'Type of failed objects (',
          in: 'query',
          name: 'type',
          required: false,
          type: ['string']
        },
        {
          description: 'Type of failures (',
          in: 'query',
          name: 'failure_type',
          required: false,
          type: ['string']
        }
      ],
      path: '/geo_nodes/current/failures',
      servers: []
    },
    putGeoNodeById: {
      method: 'PUT',
      operationId: 'putGeoNodeById',
      parameters: [
        {
          description: 'The ID of the Geo node.',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'Flag indicating if the Geo node is enabled.',
          in: 'query',
          name: 'enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'The URL to connect to the Geo node.',
          in: 'query',
          name: 'url',
          required: false,
          type: ['string']
        },
        {
          description:
            'Control the maximum concurrency of LFS/attachment backfill for this secondary node.',
          in: 'query',
          name: 'files_max_capacity',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Control the maximum concurrency of repository backfill for this secondary node.',
          in: 'query',
          name: 'repos_max_capacity',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Control the maximum concurrency of verification for this node.',
          in: 'query',
          name: 'verification_max_capacity',
          required: false,
          type: ['integer']
        }
      ],
      path: '/geo_nodes/:id',
      servers: []
    }
  },
  groupBadges: {
    deleteGroupBadgeByBadgeId: {
      method: 'DELETE',
      operationId: 'deleteGroupBadgeByBadgeId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The badge ID',
          in: 'path',
          name: 'badge_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/badges/:badge_id',
      servers: []
    },
    getGroupBadgeByBadgeId: {
      method: 'GET',
      operationId: 'getGroupBadgeByBadgeId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The badge ID',
          in: 'path',
          name: 'badge_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/badges/:badge_id',
      servers: []
    },
    getGroupBadges: {
      method: 'GET',
      operationId: 'getGroupBadges',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/groups/:id/badges',
      servers: []
    },
    getGroupBadgesRenders: {
      method: 'GET',
      operationId: 'getGroupBadgesRenders',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'URL of the badge link',
          in: 'path',
          name: 'link_url',
          required: true,
          type: ['string']
        },
        {
          description: 'URL of the badge image',
          in: 'path',
          name: 'image_url',
          required: true,
          type: ['string']
        }
      ],
      path: '/groups/:id/badges/render',
      servers: []
    },
    postGroupBadges: {
      method: 'POST',
      operationId: 'postGroupBadges',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'URL of the badge link',
          in: 'path',
          name: 'link_url',
          required: true,
          type: ['string']
        },
        {
          description: 'URL of the badge image',
          in: 'path',
          name: 'image_url',
          required: true,
          type: ['string']
        }
      ],
      path: '/groups/:id/badges',
      servers: []
    },
    putGroupBadgeByBadgeId: {
      method: 'PUT',
      operationId: 'putGroupBadgeByBadgeId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The badge ID',
          in: 'path',
          name: 'badge_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'URL of the badge link',
          in: 'query',
          name: 'link_url',
          required: false,
          type: ['string']
        },
        {
          description: 'URL of the badge image',
          in: 'query',
          name: 'image_url',
          required: false,
          type: ['string']
        }
      ],
      path: '/groups/:id/badges/:badge_id',
      servers: []
    }
  },
  groupBoards: {
    deleteGroupBoardByBoardId: {
      method: 'DELETE',
      operationId: 'deleteGroupBoardByBoardId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/boards/:board_id',
      servers: []
    },
    deleteGroupBoardListByListId: {
      method: 'DELETE',
      operationId: 'deleteGroupBoardListByListId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        },
        {
          description: "The ID of a board's list",
          in: 'path',
          name: 'list_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/boards/:board_id/lists/:list_id',
      servers: []
    },
    getGroupBoardByBoardId: {
      method: 'GET',
      operationId: 'getGroupBoardByBoardId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/boards/:board_id',
      servers: []
    },
    getGroupBoardListByListId: {
      method: 'GET',
      operationId: 'getGroupBoardListByListId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        },
        {
          description: "The ID of a board's list",
          in: 'path',
          name: 'list_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/boards/:board_id/lists/:list_id',
      servers: []
    },
    getGroupBoardLists: {
      method: 'GET',
      operationId: 'getGroupBoardLists',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/boards/:board_id/lists',
      servers: []
    },
    getGroupBoards: {
      method: 'GET',
      operationId: 'getGroupBoards',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/groups/:id/boards',
      servers: []
    },
    postGroupBoardLists: {
      method: 'POST',
      operationId: 'postGroupBoardLists',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a label',
          in: 'path',
          name: 'label_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/boards/:board_id/lists',
      servers: []
    },
    postGroupBoards: {
      method: 'POST',
      operationId: 'postGroupBoards',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the new board',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        }
      ],
      path: '/groups/:id/boards',
      servers: []
    },
    putGroupBoardByBoardId: {
      method: 'PUT',
      operationId: 'putGroupBoardByBoardId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The new name of the board',
          in: 'query',
          name: 'name',
          required: false,
          type: ['string']
        },
        {
          description: 'The assignee the board should be scoped to',
          in: 'query',
          name: 'assignee_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'The milestone the board should be scoped to',
          in: 'query',
          name: 'milestone_id',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Comma-separated list of label names which the board should be scoped to',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description:
            'The weight range from 0 to 9, to which the board should be scoped to',
          in: 'query',
          name: 'weight',
          required: false,
          type: ['integer']
        }
      ],
      path: '/groups/:id/boards/:board_id',
      servers: []
    },
    putGroupBoardListByListId: {
      method: 'PUT',
      operationId: 'putGroupBoardListByListId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a board',
          in: 'path',
          name: 'board_id',
          required: true,
          type: ['integer']
        },
        {
          description: "The ID of a board's list",
          in: 'path',
          name: 'list_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The position of the list',
          in: 'path',
          name: 'position',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/boards/:board_id/lists/:list_id',
      servers: []
    }
  },
  groupLevelVariables: {
    deleteGroupVariableByKey: {
      method: 'DELETE',
      operationId: 'deleteGroupVariableByKey',
      parameters: [
        {
          description: 'The ID of a group or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        }
      ],
      path: '/groups/:id/variables/:key',
      servers: []
    },
    getGroupVariableByKey: {
      method: 'GET',
      operationId: 'getGroupVariableByKey',
      parameters: [
        {
          description: 'The ID of a group or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        }
      ],
      path: '/groups/:id/variables/:key',
      servers: []
    },
    getGroupVariables: {
      method: 'GET',
      operationId: 'getGroupVariables',
      parameters: [
        {
          description: 'The ID of a group or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/groups/:id/variables',
      servers: []
    },
    postGroupVariables: {
      method: 'POST',
      operationId: 'postGroupVariables',
      parameters: [
        {
          description: 'The ID of a group or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'value',
          required: true,
          type: ['string']
        },
        {
          description: 'Whether the variable is protected',
          in: 'query',
          name: 'protected',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/groups/:id/variables',
      servers: []
    },
    putGroupVariableByKey: {
      method: 'PUT',
      operationId: 'putGroupVariableByKey',
      parameters: [
        {
          description: 'The ID of a group or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'value',
          required: true,
          type: ['string']
        },
        {
          description: 'Whether the variable is protected',
          in: 'query',
          name: 'protected',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/groups/:id/variables/:key',
      servers: []
    }
  },
  groupMilestones: {
    getGroupMilestones: {
      method: 'GET',
      operationId: 'getGroupMilestones',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Return only the milestones having the given ',
          in: 'query',
          name: 'iids[]',
          required: false,
          type: ['Array']
        },
        {
          description: 'Return only ',
          in: 'query',
          name: 'state',
          required: false,
          type: ['string']
        },
        {
          description:
            'Return only milestones with a title or description matching the provided string',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        }
      ],
      path: '/groups/:id/milestones',
      servers: []
    }
  },
  groups: {
    getGroupById: {
      method: 'GET',
      operationId: 'getGroupById',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Include ',
          in: 'query',
          name: 'with_custom_attributes',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Include details from projects that belong to the specified group (defaults to ',
          in: 'query',
          name: 'with_projects',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/groups/:id',
      servers: []
    },
    getGroupProjects: {
      method: 'GET',
      operationId: 'getGroupProjects',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Limit by archived status',
          in: 'query',
          name: 'archived',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by visibility ',
          in: 'query',
          name: 'visibility',
          required: false,
          type: ['string']
        },
        {
          description: 'Return projects ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return projects sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description:
            'Return list of authorized projects matching the search criteria',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        },
        {
          description:
            'Return only the ID, URL, name, and path of each project',
          in: 'query',
          name: 'simple',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by projects owned by the current user',
          in: 'query',
          name: 'owned',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by projects starred by the current user',
          in: 'query',
          name: 'starred',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by enabled issues feature',
          in: 'query',
          name: 'with_issues_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by enabled merge requests feature',
          in: 'query',
          name: 'with_merge_requests_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Include ',
          in: 'query',
          name: 'with_custom_attributes',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/groups/:id/projects',
      servers: []
    },
    postGroupProjectByProjectId: {
      method: 'POST',
      operationId: 'postGroupProjectByProjectId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID or ',
          in: 'path',
          name: 'project_id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/groups/:id/projects/:project_id',
      servers: []
    },
    postGroups: {
      method: 'POST',
      operationId: 'postGroups',
      parameters: [
        {
          description: 'The name of the group',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        },
        {
          description: 'The path of the group',
          in: 'path',
          name: 'path',
          required: true,
          type: ['string']
        },
        {
          description: "The group's description",
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: "The group's visibility. Can be ",
          in: 'query',
          name: 'visibility',
          required: false,
          type: ['string']
        },
        {
          description:
            'Enable/disable Large File Storage (LFS) for the projects in this group',
          in: 'query',
          name: 'lfs_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Allow users to request member access.',
          in: 'query',
          name: 'request_access_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'The parent group id for creating nested group.',
          in: 'query',
          name: 'parent_id',
          required: false,
          type: ['integer']
        },
        {
          description: '(admin-only) Pipeline minutes quota for this group.',
          in: 'query',
          name: 'shared_runners_minutes_limit',
          required: false,
          type: ['integer']
        }
      ],
      path: '/groups',
      servers: []
    },
    putGroupById: {
      method: 'PUT',
      operationId: 'putGroupById',
      parameters: [
        {
          description: 'The ID of the group',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The name of the group',
          in: 'query',
          name: 'name',
          required: false,
          type: ['string']
        },
        {
          description: 'The path of the group',
          in: 'query',
          name: 'path',
          required: false,
          type: ['string']
        },
        {
          description: 'The description of the group',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description:
            'Prevent adding new members to project membership within this group',
          in: 'query',
          name: 'membership_lock',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Prevent sharing a project with another group within this group',
          in: 'query',
          name: 'share_with_group_lock',
          required: false,
          type: ['boolean']
        },
        {
          description: 'The visibility level of the group. Can be ',
          in: 'query',
          name: 'visibility',
          required: false,
          type: ['string']
        },
        {
          description:
            'Enable/disable Large File Storage (LFS) for the projects in this group',
          in: 'query',
          name: 'lfs_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Allow users to request member access.',
          in: 'query',
          name: 'request_access_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: '(admin-only) Pipeline minutes quota for this group',
          in: 'query',
          name: 'shared_runners_minutes_limit',
          required: false,
          type: ['integer']
        }
      ],
      path: '/groups/:id',
      servers: []
    }
  },
  issueLinks: {
    deleteProjectIssueLinkByIssueLinkId: {
      method: 'DELETE',
      operationId: 'deleteProjectIssueLinkByIssueLinkId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of an issue relationship',
          in: 'path',
          name: 'issue_link_id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/links/:issue_link_id',
      servers: []
    },
    getProjectIssueLinks: {
      method: 'GET',
      operationId: 'getProjectIssueLinks',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/links',
      servers: []
    },
    postProjectIssueLinks: {
      method: 'POST',
      operationId: 'postProjectIssueLinks',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID or ',
          in: 'path',
          name: 'target_project_id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a target project's issue",
          in: 'path',
          name: 'target_issue_iid',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/links',
      servers: []
    }
  },
  issues: {
    deleteProjectIssueByIssueIid: {
      method: 'DELETE',
      operationId: 'deleteProjectIssueByIssueIid',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid',
      servers: []
    },
    getGroupIssues: {
      method: 'GET',
      operationId: 'getGroupIssues',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Return all issues or just those that are ',
          in: 'query',
          name: 'state',
          required: false,
          type: ['string']
        },
        {
          description:
            'Comma-separated list of label names, issues must have all labels to be returned. ',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description: 'Return only the issues having the given ',
          in: 'query',
          name: 'iids[]',
          required: false,
          type: ['Array']
        },
        {
          description: 'The milestone title. ',
          in: 'query',
          name: 'milestone',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues for the given scope: ',
          in: 'query',
          name: 'scope',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues created by the given user ',
          in: 'query',
          name: 'author_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'Return issues assigned to the given user ',
          in: 'query',
          name: 'assignee_id',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Return issues reacted by the authenticated user by the given ',
          in: 'query',
          name: 'my_reaction_emoji',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description: 'Search group issues against their ',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues created on or after the given time',
          in: 'query',
          name: 'created_after',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Return issues created on or before the given time',
          in: 'query',
          name: 'created_before',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Return issues updated on or after the given time',
          in: 'query',
          name: 'updated_after',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Return issues updated on or before the given time',
          in: 'query',
          name: 'updated_before',
          required: false,
          type: ['datetime']
        }
      ],
      path: '/groups/:id/issues',
      servers: []
    },
    getIssues: {
      method: 'GET',
      operationId: 'getIssues',
      parameters: [
        {
          description: 'Return all issues or just those that are ',
          in: 'query',
          name: 'state',
          required: false,
          type: ['string']
        },
        {
          description:
            'Comma-separated list of label names, issues must have all labels to be returned. ',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description: 'The milestone title. ',
          in: 'query',
          name: 'milestone',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues for the given scope: ',
          in: 'query',
          name: 'scope',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues created by the given user ',
          in: 'query',
          name: 'author_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'Return issues assigned to the given user ',
          in: 'query',
          name: 'assignee_id',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Return issues reacted by the authenticated user by the given ',
          in: 'query',
          name: 'my_reaction_emoji',
          required: false,
          type: ['string']
        },
        {
          description: 'Return only the issues having the given ',
          in: 'query',
          name: 'iids[]',
          required: false,
          type: ['Array']
        },
        {
          description: 'Return issues ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description: 'Search issues against their ',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues created on or after the given time',
          in: 'query',
          name: 'created_after',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Return issues created on or before the given time',
          in: 'query',
          name: 'created_before',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Return issues updated on or after the given time',
          in: 'query',
          name: 'updated_after',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Return issues updated on or before the given time',
          in: 'query',
          name: 'updated_before',
          required: false,
          type: ['datetime']
        }
      ],
      path: '/issues',
      servers: []
    },
    getProjectIssueByIssueIid: {
      method: 'GET',
      operationId: 'getProjectIssueByIssueIid',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid',
      servers: []
    },
    getProjectIssueClosedBies: {
      method: 'GET',
      operationId: 'getProjectIssueClosedBies',
      parameters: [
        {
          description: 'The ID of a project',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The internal ID of a project issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/closed_by',
      servers: []
    },
    getProjectIssueParticipants: {
      method: 'GET',
      operationId: 'getProjectIssueParticipants',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/participants',
      servers: []
    },
    getProjectIssueTimeStats: {
      method: 'GET',
      operationId: 'getProjectIssueTimeStats',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/time_stats',
      servers: []
    },
    getProjectIssueUserAgentDetails: {
      method: 'GET',
      operationId: 'getProjectIssueUserAgentDetails',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/user_agent_detail',
      servers: []
    },
    getProjectIssues: {
      method: 'GET',
      operationId: 'getProjectIssues',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Return only the milestone having the given ',
          in: 'query',
          name: 'iids[]',
          required: false,
          type: ['Array']
        },
        {
          description: 'Return all issues or just those that are ',
          in: 'query',
          name: 'state',
          required: false,
          type: ['string']
        },
        {
          description:
            'Comma-separated list of label names, issues must have all labels to be returned. ',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description: 'The milestone title. ',
          in: 'query',
          name: 'milestone',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues for the given scope: ',
          in: 'query',
          name: 'scope',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues created by the given user ',
          in: 'query',
          name: 'author_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'Return issues assigned to the given user ',
          in: 'query',
          name: 'assignee_id',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Return issues reacted by the authenticated user by the given ',
          in: 'query',
          name: 'my_reaction_emoji',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description: 'Search project issues against their ',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issues created on or after the given time',
          in: 'query',
          name: 'created_after',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Return issues created on or before the given time',
          in: 'query',
          name: 'created_before',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Return issues updated on or after the given time',
          in: 'query',
          name: 'updated_after',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Return issues updated on or before the given time',
          in: 'query',
          name: 'updated_before',
          required: false,
          type: ['datetime']
        }
      ],
      path: '/projects/:id/issues',
      servers: []
    },
    postProjectIssueAddSpentTimes: {
      method: 'POST',
      operationId: 'postProjectIssueAddSpentTimes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The duration in human format. e.g: 3h30m',
          in: 'path',
          name: 'duration',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/add_spent_time',
      servers: []
    },
    postProjectIssueMoves: {
      method: 'POST',
      operationId: 'postProjectIssueMoves',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of the new project',
          in: 'path',
          name: 'to_project_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/move',
      servers: []
    },
    postProjectIssueResetSpentTimes: {
      method: 'POST',
      operationId: 'postProjectIssueResetSpentTimes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/reset_spent_time',
      servers: []
    },
    postProjectIssueResetTimeEstimates: {
      method: 'POST',
      operationId: 'postProjectIssueResetTimeEstimates',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/reset_time_estimate',
      servers: []
    },
    postProjectIssueSubscribes: {
      method: 'POST',
      operationId: 'postProjectIssueSubscribes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/subscribe',
      servers: []
    },
    postProjectIssueTimeEstimates: {
      method: 'POST',
      operationId: 'postProjectIssueTimeEstimates',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The duration in human format. e.g: 3h30m',
          in: 'path',
          name: 'duration',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/time_estimate',
      servers: []
    },
    postProjectIssueTodos: {
      method: 'POST',
      operationId: 'postProjectIssueTodos',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/todo',
      servers: []
    },
    postProjectIssueUnsubscribes: {
      method: 'POST',
      operationId: 'postProjectIssueUnsubscribes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/unsubscribe',
      servers: []
    },
    postProjectIssues: {
      method: 'POST',
      operationId: 'postProjectIssues',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The title of an issue',
          in: 'path',
          name: 'title',
          required: true,
          type: ['string']
        },
        {
          description: 'The description of an issue',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: 'Set an issue to be confidential. Default is ',
          in: 'query',
          name: 'confidential',
          required: false,
          type: ['boolean']
        },
        {
          description: 'The ID of a user to assign issue',
          in: 'query',
          name: 'assignee_ids',
          required: false,
          type: ['Array']
        },
        {
          description: 'The global ID of a milestone to assign issue',
          in: 'query',
          name: 'milestone_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'Comma-separated label names for an issue',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description: 'Date time string, ISO 8601 formatted, e.g. ',
          in: 'query',
          name: 'created_at',
          required: false,
          type: ['string']
        },
        {
          description: 'Date time string in the format YEAR-MONTH-DAY, e.g. ',
          in: 'query',
          name: 'due_date',
          required: false,
          type: ['string']
        },
        {
          description:
            'The IID of a merge request in which to resolve all issues. This will fill the issue with a default description and mark all discussions as resolved. When passing a description or title, these values will take precedence over the default values.',
          in: 'query',
          name: 'merge_request_to_resolve_discussions_of',
          required: false,
          type: ['integer']
        },
        {
          description:
            'The ID of a discussion to resolve. This will fill in the issue with a default description and mark the discussion as resolved. Use in combination with ',
          in: 'query',
          name: 'discussion_to_resolve',
          required: false,
          type: ['string']
        },
        {
          description: 'The weight of the issue in range 0 to 9',
          in: 'query',
          name: 'weight',
          required: false,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues',
      servers: []
    },
    putProjectIssueByIssueIid: {
      method: 'PUT',
      operationId: 'putProjectIssueByIssueIid',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's issue",
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The title of an issue',
          in: 'query',
          name: 'title',
          required: false,
          type: ['string']
        },
        {
          description: 'The description of an issue',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: 'Updates an issue to be confidential',
          in: 'query',
          name: 'confidential',
          required: false,
          type: ['boolean']
        },
        {
          description: 'The ID of the user(s) to assign the issue to. Set to ',
          in: 'query',
          name: 'assignee_ids',
          required: false,
          type: ['Array']
        },
        {
          description:
            'The global ID of a milestone to assign the issue to. Set to ',
          in: 'query',
          name: 'milestone_id',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Comma-separated label names for an issue. Set to an empty string to unassign all labels.',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description: 'The state event of an issue. Set ',
          in: 'query',
          name: 'state_event',
          required: false,
          type: ['string']
        },
        {
          description: 'Date time string, ISO 8601 formatted, e.g. ',
          in: 'query',
          name: 'updated_at',
          required: false,
          type: ['string']
        },
        {
          description: 'Date time string in the format YEAR-MONTH-DAY, e.g. ',
          in: 'query',
          name: 'due_date',
          required: false,
          type: ['string']
        },
        {
          description: 'The weight of the issue in range 0 to 9',
          in: 'query',
          name: 'weight',
          required: false,
          type: ['integer']
        },
        {
          description:
            "Flag indicating if the issue's discussion is locked. If the discussion is locked only project members can add or edit comments.",
          in: 'query',
          name: 'discussion_locked',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/issues/:issue_iid',
      servers: []
    }
  },
  keys: {},
  labels: {
    deleteProjectLabels: {
      method: 'DELETE',
      operationId: 'deleteProjectLabels',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the label',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/labels',
      servers: []
    },
    getProjectLabels: {
      method: 'GET',
      operationId: 'getProjectLabels',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/labels',
      servers: []
    },
    postProjectLabelSubscribes: {
      method: 'POST',
      operationId: 'postProjectLabelSubscribes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The ID or title of a project's label",
          in: 'path',
          name: 'label_id',
          required: true,
          type: ['integer or string']
        }
      ],
      path: '/projects/:id/labels/:label_id/subscribe',
      servers: []
    },
    postProjectLabelUnsubscribes: {
      method: 'POST',
      operationId: 'postProjectLabelUnsubscribes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The ID or title of a project's label",
          in: 'path',
          name: 'label_id',
          required: true,
          type: ['integer or string']
        }
      ],
      path: '/projects/:id/labels/:label_id/unsubscribe',
      servers: []
    },
    postProjectLabels: {
      method: 'POST',
      operationId: 'postProjectLabels',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the label',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        },
        {
          description:
            "The color of the label given in 6-digit hex notation with leading '#' sign (e.g. #FFAABB) or one of the ",
          in: 'path',
          name: 'color',
          required: true,
          type: ['string']
        },
        {
          description: 'The description of the label',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description:
            'The priority of the label. Must be greater or equal than zero or ',
          in: 'query',
          name: 'priority',
          required: false,
          type: ['integer']
        }
      ],
      path: '/projects/:id/labels',
      servers: []
    },
    putProjectLabels: {
      method: 'PUT',
      operationId: 'putProjectLabels',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the existing label',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        },
        {
          description: 'The new name of the label',
          in: 'query',
          name: 'new_name',
          required: false,
          type: ['string']
        },
        {
          description:
            "The color of the label given in 6-digit hex notation with leading '#' sign (e.g. #FFAABB) or one of the ",
          in: 'query',
          name: 'color',
          required: false,
          type: ['string']
        },
        {
          description: 'The new description of the label',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description:
            'The new priority of the label. Must be greater or equal than zero or ',
          in: 'query',
          name: 'priority',
          required: false,
          type: ['integer']
        }
      ],
      path: '/projects/:id/labels',
      servers: []
    }
  },
  license: {
    postLicenses: {
      method: 'POST',
      operationId: 'postLicenses',
      parameters: [
        {
          description: 'The license string',
          in: 'path',
          name: 'license',
          required: true,
          type: ['string']
        }
      ],
      path: '/license',
      servers: []
    }
  },
  lint: {
    postLints: {
      method: 'POST',
      operationId: 'postLints',
      parameters: [
        {
          description: 'the .gitlab-ci.yaml content',
          in: 'path',
          name: 'content',
          required: true,
          type: ['string']
        }
      ],
      path: '/lint',
      servers: []
    }
  },
  markdown: {
    postApiV4Markdowns: {
      method: 'POST',
      operationId: 'postApiV4Markdowns',
      parameters: [
        {
          description: 'The markdown text to render',
          in: 'path',
          name: 'text',
          required: true,
          type: ['string']
        },
        {
          description:
            'Render text using GitLab Flavored Markdown. Default is ',
          in: 'query',
          name: 'gfm',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Use ',
          in: 'query',
          name: 'project',
          required: false,
          type: ['string']
        }
      ],
      path: '/api/v4/markdown',
      servers: []
    }
  },
  members: {
    deleteGroupMemberByUserId: {
      method: 'DELETE',
      operationId: 'deleteGroupMemberByUserId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The user ID of the member',
          in: 'path',
          name: 'user_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/members/:user_id',
      servers: []
    },
    getGroupMemberByUserId: {
      method: 'GET',
      operationId: 'getGroupMemberByUserId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The user ID of the member',
          in: 'path',
          name: 'user_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/members/:user_id',
      servers: []
    },
    getGroupMembers: {
      method: 'GET',
      operationId: 'getGroupMembers',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'A query string to search for members',
          in: 'query',
          name: 'query',
          required: false,
          type: ['string']
        }
      ],
      path: '/groups/:id/members',
      servers: []
    },
    postGroupMembers: {
      method: 'POST',
      operationId: 'postGroupMembers',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The user ID of the new member',
          in: 'path',
          name: 'user_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'A valid access level',
          in: 'path',
          name: 'access_level',
          required: true,
          type: ['integer']
        },
        {
          description: 'A date string in the format YEAR-MONTH-DAY',
          in: 'query',
          name: 'expires_at',
          required: false,
          type: ['string']
        }
      ],
      path: '/groups/:id/members',
      servers: []
    },
    putGroupMemberByUserId: {
      method: 'PUT',
      operationId: 'putGroupMemberByUserId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The user ID of the member',
          in: 'path',
          name: 'user_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'A valid access level',
          in: 'path',
          name: 'access_level',
          required: true,
          type: ['integer']
        },
        {
          description: 'A date string in the format YEAR-MONTH-DAY',
          in: 'query',
          name: 'expires_at',
          required: false,
          type: ['string']
        }
      ],
      path: '/groups/:id/members/:user_id',
      servers: []
    }
  },
  mergeRequestApprovals: {
    getProjectApprovals: {
      method: 'GET',
      operationId: 'getProjectApprovals',
      parameters: [
        {
          description: 'The ID of a project',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/approvals',
      servers: []
    },
    getProjectMergeRequestApprovals: {
      method: 'GET',
      operationId: 'getProjectMergeRequestApprovals',
      parameters: [
        {
          description: 'The ID of a project',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The IID of MR',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/approvals',
      servers: []
    },
    postProjectApprovals: {
      method: 'POST',
      operationId: 'postProjectApprovals',
      parameters: [
        {
          description: 'The ID of a project',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description:
            'How many approvals are required before an MR can be merged',
          in: 'query',
          name: 'approvals_before_merge',
          required: false,
          type: ['integer']
        },
        {
          description: 'Reset approvals on a new push',
          in: 'query',
          name: 'reset_approvals_on_push',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Allow/Disallow overriding approvers per MR',
          in: 'query',
          name: 'disable_overriding_approvers_per_merge_request',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/approvals',
      servers: []
    },
    postProjectMergeRequestApprovals: {
      method: 'POST',
      operationId: 'postProjectMergeRequestApprovals',
      parameters: [
        {
          description: 'The ID of a project',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The IID of MR',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'Approvals required before MR can be merged',
          in: 'path',
          name: 'approvals_required',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/approvals',
      servers: []
    },
    postProjectMergeRequestApproves: {
      method: 'POST',
      operationId: 'postProjectMergeRequestApproves',
      parameters: [
        {
          description: 'The ID of a project',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The IID of MR',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The HEAD of the MR',
          in: 'query',
          name: 'sha',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/approve',
      servers: []
    },
    postProjectMergeRequestUnapproves: {
      method: 'POST',
      operationId: 'postProjectMergeRequestUnapproves',
      parameters: [
        {
          description: 'The ID of a project',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The IID of MR',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/unapprove',
      servers: []
    },
    putProjectApprovers: {
      method: 'PUT',
      operationId: 'putProjectApprovers',
      parameters: [
        {
          description: 'The ID of a project',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'An array of User IDs that can approve MRs',
          in: 'path',
          name: 'approver_ids',
          required: true,
          type: ['Array']
        },
        {
          description: 'An array of Group IDs whose members can approve MRs',
          in: 'path',
          name: 'approver_group_ids',
          required: true,
          type: ['Array']
        }
      ],
      path: '/projects/:id/approvers',
      servers: []
    },
    putProjectMergeRequestApprovers: {
      method: 'PUT',
      operationId: 'putProjectMergeRequestApprovers',
      parameters: [
        {
          description: 'The ID of a project',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The IID of MR',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'An array of User IDs that can approve the MR',
          in: 'path',
          name: 'approver_ids',
          required: true,
          type: ['Array']
        },
        {
          description: 'An array of Group IDs whose members can approve the MR',
          in: 'path',
          name: 'approver_group_ids',
          required: true,
          type: ['Array']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/approvers',
      servers: []
    }
  },
  mergeRequests: {
    deleteProjectMergeRequestByMergeRequestIid: {
      method: 'DELETE',
      operationId: 'deleteProjectMergeRequestByMergeRequestIid',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID of the merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid',
      servers: []
    },
    getGroupMergeRequests: {
      method: 'GET',
      operationId: 'getGroupMergeRequests',
      parameters: [
        {
          description: 'The ID of a group',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'Return all merge requests or just those that are ',
          in: 'query',
          name: 'state',
          required: false,
          type: ['string']
        },
        {
          description: 'Return merge requests ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return merge requests sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description: 'Return merge requests for a specific milestone',
          in: 'query',
          name: 'milestone',
          required: false,
          type: ['string']
        },
        {
          description: 'If ',
          in: 'query',
          name: 'view',
          required: false,
          type: ['string']
        },
        {
          description:
            'Return merge requests matching a comma separated list of labels',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description:
            'Return merge requests created on or after the given time',
          in: 'query',
          name: 'created_after',
          required: false,
          type: ['datetime']
        },
        {
          description:
            'Return merge requests created on or before the given time',
          in: 'query',
          name: 'created_before',
          required: false,
          type: ['datetime']
        },
        {
          description:
            'Return merge requests updated on or after the given time',
          in: 'query',
          name: 'updated_after',
          required: false,
          type: ['datetime']
        },
        {
          description:
            'Return merge requests updated on or before the given time',
          in: 'query',
          name: 'updated_before',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Return merge requests for the given scope: ',
          in: 'query',
          name: 'scope',
          required: false,
          type: ['string']
        },
        {
          description: 'Returns merge requests created by the given user ',
          in: 'query',
          name: 'author_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'Returns merge requests assigned to the given user ',
          in: 'query',
          name: 'assignee_id',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Return merge requests reacted by the authenticated user by the given ',
          in: 'query',
          name: 'my_reaction_emoji',
          required: false,
          type: ['string']
        },
        {
          description: 'Return merge requests with the given source branch',
          in: 'query',
          name: 'source_branch',
          required: false,
          type: ['string']
        },
        {
          description: 'Return merge requests with the given target branch',
          in: 'query',
          name: 'target_branch',
          required: false,
          type: ['string']
        },
        {
          description: 'Search merge requests against their ',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        }
      ],
      path: '/groups/:id/merge_requests',
      servers: []
    },
    getMergeRequests: {
      method: 'GET',
      operationId: 'getMergeRequests',
      parameters: [
        {
          description: 'Return all merge requests or just those that are ',
          in: 'query',
          name: 'state',
          required: false,
          type: ['string']
        },
        {
          description: 'Return requests ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return requests sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description: 'Return merge requests for a specific milestone',
          in: 'query',
          name: 'milestone',
          required: false,
          type: ['string']
        },
        {
          description: 'If ',
          in: 'query',
          name: 'view',
          required: false,
          type: ['string']
        },
        {
          description:
            'Return merge requests matching a comma separated list of labels',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description:
            'Return merge requests created on or after the given time',
          in: 'query',
          name: 'created_after',
          required: false,
          type: ['datetime']
        },
        {
          description:
            'Return merge requests created on or before the given time',
          in: 'query',
          name: 'created_before',
          required: false,
          type: ['datetime']
        },
        {
          description:
            'Return merge requests updated on or after the given time',
          in: 'query',
          name: 'updated_after',
          required: false,
          type: ['datetime']
        },
        {
          description:
            'Return merge requests updated on or before the given time',
          in: 'query',
          name: 'updated_before',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Return merge requests for the given scope: ',
          in: 'query',
          name: 'scope',
          required: false,
          type: ['string']
        },
        {
          description: 'Returns merge requests created by the given user ',
          in: 'query',
          name: 'author_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'Returns merge requests assigned to the given user ',
          in: 'query',
          name: 'assignee_id',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Return merge requests reacted by the authenticated user by the given ',
          in: 'query',
          name: 'my_reaction_emoji',
          required: false,
          type: ['string']
        },
        {
          description: 'Return merge requests with the given source branch',
          in: 'query',
          name: 'source_branch',
          required: false,
          type: ['string']
        },
        {
          description: 'Return merge requests with the given target branch',
          in: 'query',
          name: 'target_branch',
          required: false,
          type: ['string']
        },
        {
          description: 'Search merge requests against their ',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        }
      ],
      path: '/merge_requests',
      servers: []
    },
    getProjectMergeRequestClosesIssues: {
      method: 'GET',
      operationId: 'getProjectMergeRequestClosesIssues',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID of the merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/closes_issues',
      servers: []
    },
    getProjectMergeRequestTimeStats: {
      method: 'GET',
      operationId: 'getProjectMergeRequestTimeStats',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID of the merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/time_stats',
      servers: []
    },
    getProjectMergeRequestVersionByVersionId: {
      method: 'GET',
      operationId: 'getProjectMergeRequestVersionByVersionId',
      parameters: [
        {
          description: 'The ID of the project',
          in: 'path',
          name: 'id',
          required: true,
          type: ['String']
        },
        {
          description: 'The internal ID of the merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of the merge request diff version',
          in: 'path',
          name: 'version_id',
          required: true,
          type: ['integer']
        }
      ],
      path:
        '/projects/:id/merge_requests/:merge_request_iid/versions/:version_id',
      servers: []
    },
    getProjectMergeRequestVersions: {
      method: 'GET',
      operationId: 'getProjectMergeRequestVersions',
      parameters: [
        {
          description: 'The ID of the project',
          in: 'path',
          name: 'id',
          required: true,
          type: ['String']
        },
        {
          description: 'The internal ID of the merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/versions',
      servers: []
    },
    getProjectMergeRequests: {
      method: 'GET',
      operationId: 'getProjectMergeRequests',
      parameters: [
        {
          description: 'The ID of a project',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'Return the request having the given ',
          in: 'query',
          name: 'iids[]',
          required: false,
          type: ['Array']
        },
        {
          description: 'Return all merge requests or just those that are ',
          in: 'query',
          name: 'state',
          required: false,
          type: ['string']
        },
        {
          description: 'Return requests ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return requests sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description: 'Return merge requests for a specific milestone',
          in: 'query',
          name: 'milestone',
          required: false,
          type: ['string']
        },
        {
          description: 'If ',
          in: 'query',
          name: 'view',
          required: false,
          type: ['string']
        },
        {
          description:
            'Return merge requests matching a comma separated list of labels',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description:
            'Return merge requests created on or after the given time',
          in: 'query',
          name: 'created_after',
          required: false,
          type: ['datetime']
        },
        {
          description:
            'Return merge requests created on or before the given time',
          in: 'query',
          name: 'created_before',
          required: false,
          type: ['datetime']
        },
        {
          description:
            'Return merge requests updated on or after the given time',
          in: 'query',
          name: 'updated_after',
          required: false,
          type: ['datetime']
        },
        {
          description:
            'Return merge requests updated on or before the given time',
          in: 'query',
          name: 'updated_before',
          required: false,
          type: ['datetime']
        },
        {
          description: 'Return merge requests for the given scope: ',
          in: 'query',
          name: 'scope',
          required: false,
          type: ['string']
        },
        {
          description: 'Returns merge requests created by the given user ',
          in: 'query',
          name: 'author_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'Returns merge requests assigned to the given user ',
          in: 'query',
          name: 'assignee_id',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Return merge requests reacted by the authenticated user by the given ',
          in: 'query',
          name: 'my_reaction_emoji',
          required: false,
          type: ['string']
        },
        {
          description: 'Return merge requests with the given source branch',
          in: 'query',
          name: 'source_branch',
          required: false,
          type: ['string']
        },
        {
          description: 'Return merge requests with the given target branch',
          in: 'query',
          name: 'target_branch',
          required: false,
          type: ['string']
        },
        {
          description: 'Search merge requests against their ',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/merge_requests',
      servers: []
    },
    postProjectMergeRequestAddSpentTimes: {
      method: 'POST',
      operationId: 'postProjectMergeRequestAddSpentTimes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID of the merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The duration in human format. e.g: 3h30m',
          in: 'path',
          name: 'duration',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/add_spent_time',
      servers: []
    },
    postProjectMergeRequestResetSpentTimes: {
      method: 'POST',
      operationId: 'postProjectMergeRequestResetSpentTimes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's merge_request",
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/reset_spent_time',
      servers: []
    },
    postProjectMergeRequestResetTimeEstimates: {
      method: 'POST',
      operationId: 'postProjectMergeRequestResetTimeEstimates',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "The internal ID of a project's merge_request",
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        }
      ],
      path:
        '/projects/:id/merge_requests/:merge_request_iid/reset_time_estimate',
      servers: []
    },
    postProjectMergeRequestSubscribes: {
      method: 'POST',
      operationId: 'postProjectMergeRequestSubscribes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID of the merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/subscribe',
      servers: []
    },
    postProjectMergeRequestTimeEstimates: {
      method: 'POST',
      operationId: 'postProjectMergeRequestTimeEstimates',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID of the merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The duration in human format. e.g: 3h30m',
          in: 'path',
          name: 'duration',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/time_estimate',
      servers: []
    },
    postProjectMergeRequestTodos: {
      method: 'POST',
      operationId: 'postProjectMergeRequestTodos',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID of the merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/todo',
      servers: []
    },
    postProjectMergeRequestUnsubscribes: {
      method: 'POST',
      operationId: 'postProjectMergeRequestUnsubscribes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The internal ID of the merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/unsubscribe',
      servers: []
    },
    postProjectMergeRequests: {
      method: 'POST',
      operationId: 'postProjectMergeRequests',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The source branch',
          in: 'path',
          name: 'source_branch',
          required: true,
          type: ['string']
        },
        {
          description: 'The target branch',
          in: 'path',
          name: 'target_branch',
          required: true,
          type: ['string']
        },
        {
          description: 'Title of MR',
          in: 'path',
          name: 'title',
          required: true,
          type: ['string']
        },
        {
          description: 'Assignee user ID',
          in: 'query',
          name: 'assignee_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'Description of MR',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: 'The target project (numeric id)',
          in: 'query',
          name: 'target_project_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'Labels for MR as a comma-separated list',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description: 'The ID of a milestone',
          in: 'query',
          name: 'milestone_id',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Flag indicating if a merge request should remove the source branch when merging',
          in: 'query',
          name: 'remove_source_branch',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Allow commits from members who can merge to the target branch',
          in: 'query',
          name: 'allow_collaboration',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Deprecated, see allow_collaboration',
          in: 'query',
          name: 'allow_maintainer_to_push',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Squash commits into a single commit when merging',
          in: 'query',
          name: 'squash',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/merge_requests',
      servers: []
    },
    putProjectMergeRequestByMergeRequestIid: {
      method: 'PUT',
      operationId: 'putProjectMergeRequestByMergeRequestIid',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The target branch',
          in: 'query',
          name: 'target_branch',
          required: false,
          type: ['string']
        },
        {
          description: 'Title of MR',
          in: 'query',
          name: 'title',
          required: false,
          type: ['string']
        },
        {
          description:
            'The ID of the user to assign the merge request to. Set to ',
          in: 'query',
          name: 'assignee_id',
          required: false,
          type: ['integer']
        },
        {
          description:
            'The ID of a milestone to assign the merge request to. Set to ',
          in: 'query',
          name: 'milestone_id',
          required: false,
          type: ['integer']
        },
        {
          description:
            'Comma-separated label names for a merge request. Set to an empty string to unassign all labels.',
          in: 'query',
          name: 'labels',
          required: false,
          type: ['string']
        },
        {
          description: 'Description of MR',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: 'New state (close/reopen)',
          in: 'query',
          name: 'state_event',
          required: false,
          type: ['string']
        },
        {
          description:
            'Flag indicating if a merge request should remove the source branch when merging',
          in: 'query',
          name: 'remove_source_branch',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Squash commits into a single commit when merging',
          in: 'query',
          name: 'squash',
          required: false,
          type: ['boolean']
        },
        {
          description:
            "Flag indicating if the merge request's discussion is locked. If the discussion is locked only project members can add, edit or resolve comments.",
          in: 'query',
          name: 'discussion_locked',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Allow commits from members who can merge to the target branch',
          in: 'query',
          name: 'allow_collaboration',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Deprecated, see allow_collaboration',
          in: 'query',
          name: 'allow_maintainer_to_push',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid',
      servers: []
    }
  },
  milestones: {
    getProjectMilestones: {
      method: 'GET',
      operationId: 'getProjectMilestones',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Return only the milestones having the given ',
          in: 'query',
          name: 'iids[]',
          required: false,
          type: ['Array']
        },
        {
          description: 'Return only ',
          in: 'query',
          name: 'state',
          required: false,
          type: ['string']
        },
        {
          description:
            'Return only milestones with a title or description matching the provided string',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/milestones',
      servers: []
    }
  },
  namespaces: {
    getNamespaceById: {
      method: 'GET',
      operationId: 'getNamespaceById',
      parameters: [
        {
          description: 'ID or path of the namespace',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/namespaces/:id',
      servers: []
    },
    getNamespacesSearchFoobars: {
      method: 'GET',
      operationId: 'getNamespacesSearchFoobars',
      parameters: [
        {
          description:
            'Returns a list of namespaces the user is authorized to see based on the search criteria',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        }
      ],
      path: '/namespaces?search=foobar',
      servers: []
    }
  },
  notes: {
    deleteGroupEpicNoteByNoteId: {
      method: 'DELETE',
      operationId: 'deleteGroupEpicNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an epic',
          in: 'path',
          name: 'epic_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/epics/:epic_id/notes/:note_id',
      servers: []
    },
    deleteProjectIssueNoteByNoteId: {
      method: 'DELETE',
      operationId: 'deleteProjectIssueNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of an issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/notes/:note_id',
      servers: []
    },
    deleteProjectMergeRequestNoteByNoteId: {
      method: 'DELETE',
      operationId: 'deleteProjectMergeRequestNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of a merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/notes/:note_id',
      servers: []
    },
    deleteProjectSnippetNoteByNoteId: {
      method: 'DELETE',
      operationId: 'deleteProjectSnippetNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a snippet',
          in: 'path',
          name: 'snippet_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/snippets/:snippet_id/notes/:note_id',
      servers: []
    },
    getGroupEpicNoteByNoteId: {
      method: 'GET',
      operationId: 'getGroupEpicNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an epic',
          in: 'path',
          name: 'epic_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/groups/:id/epics/:epic_id/notes/:note_id',
      servers: []
    },
    getGroupEpicNotes: {
      method: 'GET',
      operationId: 'getGroupEpicNotes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a group epic',
          in: 'path',
          name: 'epic_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'Return epic notes sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description: 'Return epic notes ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        }
      ],
      path: '/groups/:id/epics/:epic_id/notes',
      servers: []
    },
    getProjectIssueNotes: {
      method: 'GET',
      operationId: 'getProjectIssueNotes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of an issue',
          in: 'path',
          name: 'issue_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'Return issue notes sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description: 'Return issue notes ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/issues/:issue_iid/notes',
      servers: []
    },
    getProjectMergeRequestNotes: {
      method: 'GET',
      operationId: 'getProjectMergeRequestNotes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The IID of a project merge request',
          in: 'path',
          name: 'merge_request_iid',
          required: true,
          type: ['integer']
        },
        {
          description: 'Return merge request notes sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description: 'Return merge request notes ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/merge_requests/:merge_request_iid/notes',
      servers: []
    },
    getProjectSnippetNotes: {
      method: 'GET',
      operationId: 'getProjectSnippetNotes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a project snippet',
          in: 'path',
          name: 'snippet_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'Return snippet notes sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description: 'Return snippet notes ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/snippets/:snippet_id/notes',
      servers: []
    },
    postGroupEpicNotes: {
      method: 'POST',
      operationId: 'postGroupEpicNotes',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an epic',
          in: 'path',
          name: 'epic_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a note',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        }
      ],
      path: '/groups/:id/epics/:epic_id/notes',
      servers: []
    },
    putGroupEpicNoteByNoteId: {
      method: 'PUT',
      operationId: 'putGroupEpicNoteByNoteId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of an epic',
          in: 'path',
          name: 'epic_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of a note',
          in: 'path',
          name: 'note_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The content of a note',
          in: 'path',
          name: 'body',
          required: true,
          type: ['string']
        }
      ],
      path: '/groups/:id/epics/:epic_id/notes/:note_id',
      servers: []
    }
  },
  notificationSettings: {
    getGroupNotificationSettings: {
      method: 'GET',
      operationId: 'getGroupNotificationSettings',
      parameters: [
        {
          description: 'The group/project ID or path',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/groups/:id/notification_settings',
      servers: []
    },
    putGroupNotificationSettings: {
      method: 'PUT',
      operationId: 'putGroupNotificationSettings',
      parameters: [
        {
          description: 'The group/project ID or path',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The global notification level',
          in: 'query',
          name: 'level',
          required: false,
          type: ['string']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'new_note',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'new_issue',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'reopen_issue',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'close_issue',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'reassign_issue',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'issue_due',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'new_merge_request',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'push_to_merge_request',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'reopen_merge_request',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'close_merge_request',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'reassign_merge_request',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'merge_merge_request',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'failed_pipeline',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'success_pipeline',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/groups/:id/notification_settings',
      servers: []
    },
    putNotificationSettings: {
      method: 'PUT',
      operationId: 'putNotificationSettings',
      parameters: [
        {
          description: 'The global notification level',
          in: 'query',
          name: 'level',
          required: false,
          type: ['string']
        },
        {
          description: 'The email address to send notifications',
          in: 'query',
          name: 'notification_email',
          required: false,
          type: ['string']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'new_note',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'new_issue',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'reopen_issue',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'close_issue',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'reassign_issue',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'issue_due',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'new_merge_request',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'push_to_merge_request',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'reopen_merge_request',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'close_merge_request',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'reassign_merge_request',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'merge_merge_request',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'failed_pipeline',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable/disable this notification',
          in: 'query',
          name: 'success_pipeline',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/notification_settings',
      servers: []
    }
  },
  pagesDomains: {},
  pipelineSchedules: {
    deleteProjectPipelineScheduleByPipelineScheduleId: {
      method: 'DELETE',
      operationId: 'deleteProjectPipelineScheduleByPipelineScheduleId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The pipeline schedule id',
          in: 'path',
          name: 'pipeline_schedule_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/pipeline_schedules/:pipeline_schedule_id',
      servers: []
    },
    deleteProjectPipelineScheduleVariableByKey: {
      method: 'DELETE',
      operationId: 'deleteProjectPipelineScheduleVariableByKey',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The pipeline schedule id',
          in: 'path',
          name: 'pipeline_schedule_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        }
      ],
      path:
        '/projects/:id/pipeline_schedules/:pipeline_schedule_id/variables/:key',
      servers: []
    },
    getProjectPipelineScheduleByPipelineScheduleId: {
      method: 'GET',
      operationId: 'getProjectPipelineScheduleByPipelineScheduleId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The pipeline schedule id',
          in: 'path',
          name: 'pipeline_schedule_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/pipeline_schedules/:pipeline_schedule_id',
      servers: []
    },
    getProjectPipelineSchedules: {
      method: 'GET',
      operationId: 'getProjectPipelineSchedules',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The scope of pipeline schedules, one of: ',
          in: 'query',
          name: 'scope',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/pipeline_schedules',
      servers: []
    },
    postProjectPipelineScheduleTakeOwnerships: {
      method: 'POST',
      operationId: 'postProjectPipelineScheduleTakeOwnerships',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The pipeline schedule id',
          in: 'path',
          name: 'pipeline_schedule_id',
          required: true,
          type: ['integer']
        }
      ],
      path:
        '/projects/:id/pipeline_schedules/:pipeline_schedule_id/take_ownership',
      servers: []
    },
    postProjectPipelineScheduleVariables: {
      method: 'POST',
      operationId: 'postProjectPipelineScheduleVariables',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The pipeline schedule id',
          in: 'path',
          name: 'pipeline_schedule_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'value',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/pipeline_schedules/:pipeline_schedule_id/variables',
      servers: []
    },
    postProjectPipelineSchedules: {
      method: 'POST',
      operationId: 'postProjectPipelineSchedules',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The description of pipeline schedule',
          in: 'path',
          name: 'description',
          required: true,
          type: ['string']
        },
        {
          description: 'The branch/tag name will be triggered',
          in: 'path',
          name: 'ref',
          required: true,
          type: ['string']
        },
        {
          description: 'The cron (e.g. ',
          in: 'path',
          name: 'cron',
          required: true,
          type: ['string']
        },
        {
          description: 'The timezone supported by ',
          in: 'query',
          name: 'cron_timezone',
          required: false,
          type: ['string']
        },
        {
          description:
            'The activation of pipeline schedule. If false is set, the pipeline schedule will deactivated initially (default: ',
          in: 'query',
          name: 'active',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/pipeline_schedules',
      servers: []
    },
    putProjectPipelineScheduleByPipelineScheduleId: {
      method: 'PUT',
      operationId: 'putProjectPipelineScheduleByPipelineScheduleId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The pipeline schedule id',
          in: 'path',
          name: 'pipeline_schedule_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The description of pipeline schedule',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: 'The branch/tag name will be triggered',
          in: 'query',
          name: 'ref',
          required: false,
          type: ['string']
        },
        {
          description: 'The cron (e.g. ',
          in: 'query',
          name: 'cron',
          required: false,
          type: ['string']
        },
        {
          description: 'The timezone supported by ',
          in: 'query',
          name: 'cron_timezone',
          required: false,
          type: ['string']
        },
        {
          description:
            'The activation of pipeline schedule. If false is set, the pipeline schedule will deactivated initially.',
          in: 'query',
          name: 'active',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/pipeline_schedules/:pipeline_schedule_id',
      servers: []
    },
    putProjectPipelineScheduleVariableByKey: {
      method: 'PUT',
      operationId: 'putProjectPipelineScheduleVariableByKey',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The pipeline schedule id',
          in: 'path',
          name: 'pipeline_schedule_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'value',
          required: true,
          type: ['string']
        }
      ],
      path:
        '/projects/:id/pipeline_schedules/:pipeline_schedule_id/variables/:key',
      servers: []
    }
  },
  pipelineTriggers: {
    deleteProjectTriggerByTriggerId: {
      method: 'DELETE',
      operationId: 'deleteProjectTriggerByTriggerId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The trigger id',
          in: 'path',
          name: 'trigger_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/triggers/:trigger_id',
      servers: []
    },
    getProjectTriggerByTriggerId: {
      method: 'GET',
      operationId: 'getProjectTriggerByTriggerId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The trigger id',
          in: 'path',
          name: 'trigger_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/triggers/:trigger_id',
      servers: []
    },
    getProjectTriggers: {
      method: 'GET',
      operationId: 'getProjectTriggers',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/triggers',
      servers: []
    },
    postProjectTriggerTakeOwnerships: {
      method: 'POST',
      operationId: 'postProjectTriggerTakeOwnerships',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The trigger id',
          in: 'path',
          name: 'trigger_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/triggers/:trigger_id/take_ownership',
      servers: []
    },
    postProjectTriggers: {
      method: 'POST',
      operationId: 'postProjectTriggers',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The trigger name',
          in: 'path',
          name: 'description',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/triggers',
      servers: []
    },
    putProjectTriggerByTriggerId: {
      method: 'PUT',
      operationId: 'putProjectTriggerByTriggerId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The trigger id',
          in: 'path',
          name: 'trigger_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The trigger name',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/triggers/:trigger_id',
      servers: []
    }
  },
  pipelines: {
    getProjectPipelineByPipelineId: {
      method: 'GET',
      operationId: 'getProjectPipelineByPipelineId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a pipeline',
          in: 'path',
          name: 'pipeline_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/pipelines/:pipeline_id',
      servers: []
    },
    getProjectPipelines: {
      method: 'GET',
      operationId: 'getProjectPipelines',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The scope of pipelines, one of: ',
          in: 'query',
          name: 'scope',
          required: false,
          type: ['string']
        },
        {
          description: 'The status of pipelines, one of: ',
          in: 'query',
          name: 'status',
          required: false,
          type: ['string']
        },
        {
          description: 'The ref of pipelines',
          in: 'query',
          name: 'ref',
          required: false,
          type: ['string']
        },
        {
          description: 'The sha or pipelines',
          in: 'query',
          name: 'sha',
          required: false,
          type: ['string']
        },
        {
          description: 'Returns pipelines with invalid configurations',
          in: 'query',
          name: 'yaml_errors',
          required: false,
          type: ['boolean']
        },
        {
          description: 'The name of the user who triggered pipelines',
          in: 'query',
          name: 'name',
          required: false,
          type: ['string']
        },
        {
          description: 'The username of the user who triggered pipelines',
          in: 'query',
          name: 'username',
          required: false,
          type: ['string']
        },
        {
          description: 'Order pipelines by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Sort pipelines in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/pipelines',
      servers: []
    },
    postProjectPipelineCancels: {
      method: 'POST',
      operationId: 'postProjectPipelineCancels',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a pipeline',
          in: 'path',
          name: 'pipeline_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/pipelines/:pipeline_id/cancel',
      servers: []
    },
    postProjectPipelineRetries: {
      method: 'POST',
      operationId: 'postProjectPipelineRetries',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a pipeline',
          in: 'path',
          name: 'pipeline_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/pipelines/:pipeline_id/retry',
      servers: []
    },
    postProjectPipelines: {
      method: 'POST',
      operationId: 'postProjectPipelines',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Reference to commit',
          in: 'path',
          name: 'ref',
          required: true,
          type: ['string']
        },
        {
          description:
            'An array containing the variables available in the pipeline, matching the structure ',
          in: 'query',
          name: 'variables',
          required: false,
          type: ['array']
        }
      ],
      path: '/projects/:id/pipeline',
      servers: []
    }
  },
  projectBadges: {
    deleteProjectBadgeByBadgeId: {
      method: 'DELETE',
      operationId: 'deleteProjectBadgeByBadgeId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The badge ID',
          in: 'path',
          name: 'badge_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/badges/:badge_id',
      servers: []
    },
    getProjectBadgeByBadgeId: {
      method: 'GET',
      operationId: 'getProjectBadgeByBadgeId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The badge ID',
          in: 'path',
          name: 'badge_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/badges/:badge_id',
      servers: []
    },
    getProjectBadges: {
      method: 'GET',
      operationId: 'getProjectBadges',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/badges',
      servers: []
    },
    getProjectBadgesRenders: {
      method: 'GET',
      operationId: 'getProjectBadgesRenders',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'URL of the badge link',
          in: 'path',
          name: 'link_url',
          required: true,
          type: ['string']
        },
        {
          description: 'URL of the badge image',
          in: 'path',
          name: 'image_url',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/badges/render',
      servers: []
    },
    postProjectBadges: {
      method: 'POST',
      operationId: 'postProjectBadges',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'URL of the badge link',
          in: 'path',
          name: 'link_url',
          required: true,
          type: ['string']
        },
        {
          description: 'URL of the badge image',
          in: 'path',
          name: 'image_url',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/badges',
      servers: []
    },
    putProjectBadgeByBadgeId: {
      method: 'PUT',
      operationId: 'putProjectBadgeByBadgeId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The badge ID',
          in: 'path',
          name: 'badge_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'URL of the badge link',
          in: 'query',
          name: 'link_url',
          required: false,
          type: ['string']
        },
        {
          description: 'URL of the badge image',
          in: 'query',
          name: 'image_url',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/badges/:badge_id',
      servers: []
    }
  },
  projectImportExport: {},
  projectLevelVariables: {
    deleteProjectVariableByKey: {
      method: 'DELETE',
      operationId: 'deleteProjectVariableByKey',
      parameters: [
        {
          description: 'The ID of a project or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/variables/:key',
      servers: []
    },
    getProjectVariableByKey: {
      method: 'GET',
      operationId: 'getProjectVariableByKey',
      parameters: [
        {
          description: 'The ID of a project or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/variables/:key',
      servers: []
    },
    getProjectVariables: {
      method: 'GET',
      operationId: 'getProjectVariables',
      parameters: [
        {
          description: 'The ID of a project or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/variables',
      servers: []
    },
    postProjectVariables: {
      method: 'POST',
      operationId: 'postProjectVariables',
      parameters: [
        {
          description: 'The ID of a project or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'value',
          required: true,
          type: ['string']
        },
        {
          description: 'Whether the variable is protected',
          in: 'query',
          name: 'protected',
          required: false,
          type: ['boolean']
        },
        {
          description: 'The ',
          in: 'query',
          name: 'environment_scope',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/variables',
      servers: []
    },
    putProjectVariableByKey: {
      method: 'PUT',
      operationId: 'putProjectVariableByKey',
      parameters: [
        {
          description: 'The ID of a project or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'value',
          required: true,
          type: ['string']
        },
        {
          description: 'Whether the variable is protected',
          in: 'query',
          name: 'protected',
          required: false,
          type: ['boolean']
        },
        {
          description: 'The ',
          in: 'query',
          name: 'environment_scope',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/variables/:key',
      servers: []
    }
  },
  projectSnippets: {
    getProjectSnippetUserAgentDetails: {
      method: 'GET',
      operationId: 'getProjectSnippetUserAgentDetails',
      parameters: [
        {
          description: 'The ID of a project',
          in: 'path',
          name: 'id',
          required: true,
          type: ['Integer']
        },
        {
          description: 'The ID of a snippet',
          in: 'path',
          name: 'snippet_id',
          required: true,
          type: ['Integer']
        }
      ],
      path: '/projects/:id/snippets/:snippet_id/user_agent_detail',
      servers: []
    }
  },
  projects: {
    deleteProjectById: {
      method: 'DELETE',
      operationId: 'deleteProjectById',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id',
      servers: []
    },
    deleteProjectForks: {
      method: 'DELETE',
      operationId: 'deleteProjectForks',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/fork',
      servers: []
    },
    deleteProjectHookByHookId: {
      method: 'DELETE',
      operationId: 'deleteProjectHookByHookId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of the project hook',
          in: 'path',
          name: 'hook_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/hooks/:hook_id',
      servers: []
    },
    deleteProjectPushRules: {
      method: 'DELETE',
      operationId: 'deleteProjectPushRules',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/push_rule',
      servers: []
    },
    deleteProjectShareByGroupId: {
      method: 'DELETE',
      operationId: 'deleteProjectShareByGroupId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of the group',
          in: 'path',
          name: 'group_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/share/:group_id',
      servers: []
    },
    getProjectById: {
      method: 'GET',
      operationId: 'getProjectById',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Include project statistics',
          in: 'query',
          name: 'statistics',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Include ',
          in: 'query',
          name: 'with_custom_attributes',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id',
      servers: []
    },
    getProjectForks: {
      method: 'GET',
      operationId: 'getProjectForks',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Limit by archived status',
          in: 'query',
          name: 'archived',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by visibility ',
          in: 'query',
          name: 'visibility',
          required: false,
          type: ['string']
        },
        {
          description: 'Return projects ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return projects sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description: 'Return list of projects matching the search criteria',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        },
        {
          description:
            'Return only limited fields for each project. This is a no-op without authentication as then ',
          in: 'query',
          name: 'simple',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by projects explicitly owned by the current user',
          in: 'query',
          name: 'owned',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by projects that the current user is a member of',
          in: 'query',
          name: 'membership',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by projects starred by the current user',
          in: 'query',
          name: 'starred',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Include project statistics',
          in: 'query',
          name: 'statistics',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Include ',
          in: 'query',
          name: 'with_custom_attributes',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by enabled issues feature',
          in: 'query',
          name: 'with_issues_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by enabled merge requests feature',
          in: 'query',
          name: 'with_merge_requests_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by current user minimal ',
          in: 'query',
          name: 'min_access_level',
          required: false,
          type: ['integer']
        }
      ],
      path: '/projects/:id/forks',
      servers: []
    },
    getProjectHookByHookId: {
      method: 'GET',
      operationId: 'getProjectHookByHookId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of a project hook',
          in: 'path',
          name: 'hook_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/projects/:id/hooks/:hook_id',
      servers: []
    },
    getProjectHooks: {
      method: 'GET',
      operationId: 'getProjectHooks',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/hooks',
      servers: []
    },
    getProjectPushRules: {
      method: 'GET',
      operationId: 'getProjectPushRules',
      parameters: [
        {
          description: 'The ID of the project or NAMESPACE/PROJECT_NAME',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/push_rule',
      servers: []
    },
    getProjectSnapshots: {
      method: 'GET',
      operationId: 'getProjectSnapshots',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description:
            'Whether to download the wiki, rather than project, repository',
          in: 'query',
          name: 'wiki',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/snapshot',
      servers: []
    },
    getProjectUsers: {
      method: 'GET',
      operationId: 'getProjectUsers',
      parameters: [
        {
          description: 'Search for specific users',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/users',
      servers: []
    },
    getProjects: {
      method: 'GET',
      operationId: 'getProjects',
      parameters: [
        {
          description: 'A string contained in the project name',
          in: 'path',
          name: 'search',
          required: true,
          type: ['string']
        },
        {
          description: 'Return requests ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return requests sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects',
      servers: []
    },
    getUserProjects: {
      method: 'GET',
      operationId: 'getUserProjects',
      parameters: [
        {
          description: 'The ID or username of the user',
          in: 'path',
          name: 'user_id',
          required: true,
          type: ['string']
        },
        {
          description: 'Limit by archived status',
          in: 'query',
          name: 'archived',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by visibility ',
          in: 'query',
          name: 'visibility',
          required: false,
          type: ['string']
        },
        {
          description: 'Return projects ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return projects sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description: 'Return list of projects matching the search criteria',
          in: 'query',
          name: 'search',
          required: false,
          type: ['string']
        },
        {
          description:
            'Return only limited fields for each project. This is a no-op without authentication as then ',
          in: 'query',
          name: 'simple',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by projects explicitly owned by the current user',
          in: 'query',
          name: 'owned',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by projects that the current user is a member of',
          in: 'query',
          name: 'membership',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by projects starred by the current user',
          in: 'query',
          name: 'starred',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Include project statistics',
          in: 'query',
          name: 'statistics',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Include ',
          in: 'query',
          name: 'with_custom_attributes',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by enabled issues feature',
          in: 'query',
          name: 'with_issues_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by enabled merge requests feature',
          in: 'query',
          name: 'with_merge_requests_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Limit by current user minimal ',
          in: 'query',
          name: 'min_access_level',
          required: false,
          type: ['integer']
        }
      ],
      path: '/users/:user_id/projects',
      servers: []
    },
    postProjectArchives: {
      method: 'POST',
      operationId: 'postProjectArchives',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/archive',
      servers: []
    },
    postProjectForkByForkedFromId: {
      method: 'POST',
      operationId: 'postProjectForkByForkedFromId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of the project that was forked from',
          in: 'path',
          name: 'forked_from_id',
          required: true,
          type: ['ID']
        }
      ],
      path: '/projects/:id/fork/:forked_from_id',
      servers: []
    },
    postProjectForks: {
      method: 'POST',
      operationId: 'postProjectForks',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description:
            'The ID or path of the namespace that the project will be forked to',
          in: 'path',
          name: 'namespace',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/fork',
      servers: []
    },
    postProjectHooks: {
      method: 'POST',
      operationId: 'postProjectHooks',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The hook URL',
          in: 'path',
          name: 'url',
          required: true,
          type: ['string']
        },
        {
          description: 'Trigger hook on push events',
          in: 'query',
          name: 'push_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on issues events',
          in: 'query',
          name: 'issues_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on confidential issues events',
          in: 'query',
          name: 'confidential_issues_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on merge requests events',
          in: 'query',
          name: 'merge_requests_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on tag push events',
          in: 'query',
          name: 'tag_push_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on note events',
          in: 'query',
          name: 'note_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on job events',
          in: 'query',
          name: 'job_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on pipeline events',
          in: 'query',
          name: 'pipeline_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on wiki events',
          in: 'query',
          name: 'wiki_page_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Do SSL verification when triggering the hook',
          in: 'query',
          name: 'enable_ssl_verification',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Secret token to validate received payloads; this will not be returned in the response',
          in: 'query',
          name: 'token',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/hooks',
      servers: []
    },
    postProjectHousekeepings: {
      method: 'POST',
      operationId: 'postProjectHousekeepings',
      parameters: [
        {
          description: 'The ID of the project or NAMESPACE/PROJECT_NAME',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/housekeeping',
      servers: []
    },
    postProjectMirrorPulls: {
      method: 'POST',
      operationId: 'postProjectMirrorPulls',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/mirror/pull',
      servers: []
    },
    postProjectPushRules: {
      method: 'POST',
      operationId: 'postProjectPushRules',
      parameters: [
        {
          description: 'The ID of the project or NAMESPACE/PROJECT_NAME',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Deny deleting a tag',
          in: 'query',
          name: 'deny_delete_tag',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Restrict commits by author (email) to existing GitLab users',
          in: 'query',
          name: 'member_check',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'GitLab will reject any files that are likely to contain secrets',
          in: 'query',
          name: 'prevent_secrets',
          required: false,
          type: ['boolean']
        },
        {
          description: 'All commit messages must match this, e.g. ',
          in: 'query',
          name: 'commit_message_regex',
          required: false,
          type: ['string']
        },
        {
          description: 'All branch names must match this, e.g. ',
          in: 'query',
          name: 'branch_name_regex',
          required: false,
          type: ['string']
        },
        {
          description: 'All commit author emails must match this, e.g. ',
          in: 'query',
          name: 'author_email_regex',
          required: false,
          type: ['string']
        },
        {
          description: 'All commited filenames must ',
          in: 'query',
          name: 'file_name_regex',
          required: false,
          type: ['string']
        },
        {
          description: 'Maximum file size (MB)',
          in: 'query',
          name: 'max_file_size',
          required: false,
          type: ['integer']
        }
      ],
      path: '/projects/:id/push_rule',
      servers: []
    },
    postProjectShares: {
      method: 'POST',
      operationId: 'postProjectShares',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of the group to share with',
          in: 'path',
          name: 'group_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ',
          in: 'path',
          name: 'group_access',
          required: true,
          type: ['integer']
        },
        {
          description: 'Share expiration date in ISO 8601 format: 2016-09-26',
          in: 'query',
          name: 'expires_at',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/share',
      servers: []
    },
    postProjectStars: {
      method: 'POST',
      operationId: 'postProjectStars',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/star',
      servers: []
    },
    postProjectUnarchives: {
      method: 'POST',
      operationId: 'postProjectUnarchives',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/unarchive',
      servers: []
    },
    postProjectUnstars: {
      method: 'POST',
      operationId: 'postProjectUnstars',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/unstar',
      servers: []
    },
    postProjectUploads: {
      method: 'POST',
      operationId: 'postProjectUploads',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The file to be uploaded',
          in: 'path',
          name: 'file',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/uploads',
      servers: []
    },
    postProjects: {
      method: 'POST',
      operationId: 'postProjects',
      parameters: [
        {
          description:
            'The name of the new project. Equals path if not provided.',
          in: 'query',
          name: 'name',
          required: false,
          type: ['string']
        },
        {
          description:
            'Repository name for new project. Generated based on name if not provided (generated lowercased with dashes).',
          in: 'query',
          name: 'path',
          required: false,
          type: ['string']
        },
        {
          description:
            "Namespace for the new project (defaults to the current user's namespace)",
          in: 'query',
          name: 'namespace_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'master',
          in: 'query',
          name: 'default_branch',
          required: false,
          type: ['string']
        },
        {
          description: 'Short project description',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: 'Enable issues for this project',
          in: 'query',
          name: 'issues_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable merge requests for this project',
          in: 'query',
          name: 'merge_requests_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable jobs for this project',
          in: 'query',
          name: 'jobs_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable wiki for this project',
          in: 'query',
          name: 'wiki_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable snippets for this project',
          in: 'query',
          name: 'snippets_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Automatically resolve merge request diffs discussions on lines changed with a push',
          in: 'query',
          name: 'resolve_outdated_diff_discussions',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable container registry for this project',
          in: 'query',
          name: 'container_registry_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable shared runners for this project',
          in: 'query',
          name: 'shared_runners_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'See ',
          in: 'query',
          name: 'visibility',
          required: false,
          type: ['string']
        },
        {
          description: 'URL to import repository from',
          in: 'query',
          name: 'import_url',
          required: false,
          type: ['string']
        },
        {
          description: 'If ',
          in: 'query',
          name: 'public_jobs',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Set whether merge requests can only be merged with successful jobs',
          in: 'query',
          name: 'only_allow_merge_if_pipeline_succeeds',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Set whether merge requests can only be merged when all the discussions are resolved',
          in: 'query',
          name: 'only_allow_merge_if_all_discussions_are_resolved',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Set the merge method used',
          in: 'query',
          name: 'merge_method',
          required: false,
          type: ['string']
        },
        {
          description: 'Enable LFS',
          in: 'query',
          name: 'lfs_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Allow users to request member access',
          in: 'query',
          name: 'request_access_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'The list of tags for a project; put array of tags, that should be finally assigned to a project',
          in: 'query',
          name: 'tag_list',
          required: false,
          type: ['array']
        },
        {
          description: 'Image file for avatar of the project',
          in: 'query',
          name: 'avatar',
          required: false,
          type: ['mixed']
        },
        {
          description:
            'Show link to create/view merge request when pushing from the command line',
          in: 'query',
          name: 'printing_merge_request_link_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'The path to CI config file',
          in: 'query',
          name: 'ci_config_path',
          required: false,
          type: ['string']
        },
        {
          description:
            'Which storage shard the repository is on. Available only to admins',
          in: 'query',
          name: 'repository_storage',
          required: false,
          type: ['string']
        },
        {
          description:
            'How many approvers should approve merge request by default',
          in: 'query',
          name: 'approvals_before_merge',
          required: false,
          type: ['integer']
        }
      ],
      path: '/projects',
      servers: []
    },
    postProjectsUserByUserId: {
      method: 'POST',
      operationId: 'postProjectsUserByUserId',
      parameters: [
        {
          description: 'The user ID of the project owner',
          in: 'path',
          name: 'user_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The name of the new project',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        },
        {
          description:
            'Custom repository name for new project. By default generated based on name',
          in: 'query',
          name: 'path',
          required: false,
          type: ['string']
        },
        {
          description:
            "Namespace for the new project (defaults to the current user's namespace)",
          in: 'query',
          name: 'namespace_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'Short project description',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: 'Enable issues for this project',
          in: 'query',
          name: 'issues_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable merge requests for this project',
          in: 'query',
          name: 'merge_requests_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable jobs for this project',
          in: 'query',
          name: 'jobs_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable wiki for this project',
          in: 'query',
          name: 'wiki_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable snippets for this project',
          in: 'query',
          name: 'snippets_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Automatically resolve merge request diffs discussions on lines changed with a push',
          in: 'query',
          name: 'resolve_outdated_diff_discussions',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable container registry for this project',
          in: 'query',
          name: 'container_registry_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable shared runners for this project',
          in: 'query',
          name: 'shared_runners_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'See ',
          in: 'query',
          name: 'visibility',
          required: false,
          type: ['string']
        },
        {
          description: 'URL to import repository from',
          in: 'query',
          name: 'import_url',
          required: false,
          type: ['string']
        },
        {
          description: 'If ',
          in: 'query',
          name: 'public_jobs',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Set whether merge requests can only be merged with successful jobs',
          in: 'query',
          name: 'only_allow_merge_if_pipeline_succeeds',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Set whether merge requests can only be merged when all the discussions are resolved',
          in: 'query',
          name: 'only_allow_merge_if_all_discussions_are_resolved',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Set the merge method used',
          in: 'query',
          name: 'merge_method',
          required: false,
          type: ['string']
        },
        {
          description: 'Enable LFS',
          in: 'query',
          name: 'lfs_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Allow users to request member access',
          in: 'query',
          name: 'request_access_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'The list of tags for a project; put array of tags, that should be finally assigned to a project',
          in: 'query',
          name: 'tag_list',
          required: false,
          type: ['array']
        },
        {
          description: 'Image file for avatar of the project',
          in: 'query',
          name: 'avatar',
          required: false,
          type: ['mixed']
        },
        {
          description:
            'Show link to create/view merge request when pushing from the command line',
          in: 'query',
          name: 'printing_merge_request_link_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'The path to CI config file',
          in: 'query',
          name: 'ci_config_path',
          required: false,
          type: ['string']
        },
        {
          description:
            'Which storage shard the repository is on. Available only to admins',
          in: 'query',
          name: 'repository_storage',
          required: false,
          type: ['string']
        },
        {
          description:
            'How many approvers should approve merge request by default',
          in: 'query',
          name: 'approvals_before_merge',
          required: false,
          type: ['integer']
        },
        {
          description: 'The classification label for the project',
          in: 'query',
          name: 'external_authorization_classification_label',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/user/:user_id',
      servers: []
    },
    putProjectById: {
      method: 'PUT',
      operationId: 'putProjectById',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the project',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        },
        {
          description:
            'Custom repository name for the project. By default generated based on name',
          in: 'query',
          name: 'path',
          required: false,
          type: ['string']
        },
        {
          description: 'master',
          in: 'query',
          name: 'default_branch',
          required: false,
          type: ['string']
        },
        {
          description: 'Short project description',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: 'Enable issues for this project',
          in: 'query',
          name: 'issues_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable merge requests for this project',
          in: 'query',
          name: 'merge_requests_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable jobs for this project',
          in: 'query',
          name: 'jobs_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable wiki for this project',
          in: 'query',
          name: 'wiki_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable snippets for this project',
          in: 'query',
          name: 'snippets_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Automatically resolve merge request diffs discussions on lines changed with a push',
          in: 'query',
          name: 'resolve_outdated_diff_discussions',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable container registry for this project',
          in: 'query',
          name: 'container_registry_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable shared runners for this project',
          in: 'query',
          name: 'shared_runners_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'See ',
          in: 'query',
          name: 'visibility',
          required: false,
          type: ['string']
        },
        {
          description: 'URL to import repository from',
          in: 'query',
          name: 'import_url',
          required: false,
          type: ['string']
        },
        {
          description: 'If ',
          in: 'query',
          name: 'public_jobs',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Set whether merge requests can only be merged with successful jobs',
          in: 'query',
          name: 'only_allow_merge_if_pipeline_succeeds',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Set whether merge requests can only be merged when all the discussions are resolved',
          in: 'query',
          name: 'only_allow_merge_if_all_discussions_are_resolved',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Set the merge method used',
          in: 'query',
          name: 'merge_method',
          required: false,
          type: ['string']
        },
        {
          description: 'Enable LFS',
          in: 'query',
          name: 'lfs_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Allow users to request member access',
          in: 'query',
          name: 'request_access_enabled',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'The list of tags for a project; put array of tags, that should be finally assigned to a project',
          in: 'query',
          name: 'tag_list',
          required: false,
          type: ['array']
        },
        {
          description: 'Image file for avatar of the project',
          in: 'query',
          name: 'avatar',
          required: false,
          type: ['mixed']
        },
        {
          description: 'The path to CI config file',
          in: 'query',
          name: 'ci_config_path',
          required: false,
          type: ['string']
        },
        {
          description:
            'Which storage shard the repository is on. Available only to admins',
          in: 'query',
          name: 'repository_storage',
          required: false,
          type: ['string']
        },
        {
          description:
            'How many approvers should approve merge request by default',
          in: 'query',
          name: 'approvals_before_merge',
          required: false,
          type: ['integer']
        },
        {
          description: 'The classification label for the project',
          in: 'query',
          name: 'external_authorization_classification_label',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id',
      servers: []
    },
    putProjectHookByHookId: {
      method: 'PUT',
      operationId: 'putProjectHookByHookId',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The ID of the project hook',
          in: 'path',
          name: 'hook_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The hook URL',
          in: 'path',
          name: 'url',
          required: true,
          type: ['string']
        },
        {
          description: 'Trigger hook on push events',
          in: 'query',
          name: 'push_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on issues events',
          in: 'query',
          name: 'issues_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on confidential issues events',
          in: 'query',
          name: 'confidential_issues_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on merge requests events',
          in: 'query',
          name: 'merge_requests_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on tag push events',
          in: 'query',
          name: 'tag_push_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on note events',
          in: 'query',
          name: 'note_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on job events',
          in: 'query',
          name: 'job_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on pipeline events',
          in: 'query',
          name: 'pipeline_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on wiki events',
          in: 'query',
          name: 'wiki_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Do SSL verification when triggering the hook',
          in: 'query',
          name: 'enable_ssl_verification',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Secret token to validate received payloads; this will not be returned in the response',
          in: 'query',
          name: 'token',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/hooks/:hook_id',
      servers: []
    },
    putProjectPushRules: {
      method: 'PUT',
      operationId: 'putProjectPushRules',
      parameters: [
        {
          description: 'The ID of the project or NAMESPACE/PROJECT_NAME',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Deny deleting a tag',
          in: 'query',
          name: 'deny_delete_tag',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'Restrict commits by author (email) to existing GitLab users',
          in: 'query',
          name: 'member_check',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'GitLab will reject any files that are likely to contain secrets',
          in: 'query',
          name: 'prevent_secrets',
          required: false,
          type: ['boolean']
        },
        {
          description: 'All commit messages must match this, e.g. ',
          in: 'query',
          name: 'commit_message_regex',
          required: false,
          type: ['string']
        },
        {
          description: 'All branch names must match this, e.g. ',
          in: 'query',
          name: 'branch_name_regex',
          required: false,
          type: ['string']
        },
        {
          description: 'All commit author emails must match this, e.g. ',
          in: 'query',
          name: 'author_email_regex',
          required: false,
          type: ['string']
        },
        {
          description: 'All commited filenames must ',
          in: 'query',
          name: 'file_name_regex',
          required: false,
          type: ['string']
        },
        {
          description: 'Maximum file size (MB)',
          in: 'query',
          name: 'max_file_size',
          required: false,
          type: ['integer']
        }
      ],
      path: '/projects/:id/push_rule',
      servers: []
    },
    putProjectTransfers: {
      method: 'PUT',
      operationId: 'putProjectTransfers',
      parameters: [
        {
          description:
            'The ID or path of the namespace to transfer to project to',
          in: 'path',
          name: 'namespace',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/transfer',
      servers: []
    }
  },
  protectedBranches: {
    deleteProjectProtectedBranchByName: {
      method: 'DELETE',
      operationId: 'deleteProjectProtectedBranchByName',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the branch',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/protected_branches/:name',
      servers: []
    },
    getProjectProtectedBranchByName: {
      method: 'GET',
      operationId: 'getProjectProtectedBranchByName',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the branch or wildcard',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/protected_branches/:name',
      servers: []
    },
    getProjectProtectedBranches: {
      method: 'GET',
      operationId: 'getProjectProtectedBranches',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        }
      ],
      path: '/projects/:id/protected_branches',
      servers: []
    },
    postProjectProtectedBranches: {
      method: 'POST',
      operationId: 'postProjectProtectedBranches',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the branch or wildcard',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        },
        {
          description: 'Access levels allowed to push (defaults: ',
          in: 'query',
          name: 'push_access_level',
          required: false,
          type: ['string']
        },
        {
          description: 'Access levels allowed to merge (defaults: ',
          in: 'query',
          name: 'merge_access_level',
          required: false,
          type: ['string']
        },
        {
          description: 'Access levels allowed to unprotect (defaults: ',
          in: 'query',
          name: 'unprotect_access_level',
          required: false,
          type: ['string']
        },
        {
          description:
            'Array of access levels allowed to push, with each described by a hash',
          in: 'query',
          name: 'allowed_to_push',
          required: false,
          type: ['array']
        },
        {
          description:
            'Array of access levels allowed to merge, with each described by a hash',
          in: 'query',
          name: 'allowed_to_merge',
          required: false,
          type: ['array']
        },
        {
          description:
            'Array of access levels allowed to unprotect, with each described by a hash',
          in: 'query',
          name: 'allowed_to_unprotect',
          required: false,
          type: ['array']
        }
      ],
      path: '/projects/:id/protected_branches',
      servers: []
    }
  },
  repositories: {},
  repositoryFiles: {},
  search: {
    getGroupSearches: {
      method: 'GET',
      operationId: 'getGroupSearches',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The scope to search in',
          in: 'path',
          name: 'scope',
          required: true,
          type: ['string']
        },
        {
          description: 'The search query',
          in: 'path',
          name: 'search',
          required: true,
          type: ['string']
        }
      ],
      path: '/groups/:id/search',
      servers: []
    },
    getProjectSearches: {
      method: 'GET',
      operationId: 'getProjectSearches',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The scope to search in',
          in: 'path',
          name: 'scope',
          required: true,
          type: ['string']
        },
        {
          description: 'The search query',
          in: 'path',
          name: 'search',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/search',
      servers: []
    },
    getSearches: {
      method: 'GET',
      operationId: 'getSearches',
      parameters: [
        {
          description: 'The scope to search in',
          in: 'path',
          name: 'scope',
          required: true,
          type: ['string']
        },
        {
          description: 'The search query',
          in: 'path',
          name: 'search',
          required: true,
          type: ['string']
        }
      ],
      path: '/search',
      servers: []
    }
  },
  services: {
    putProjectServicesAsanas: {
      method: 'PUT',
      operationId: 'putProjectServicesAsanas',
      parameters: [
        {
          description:
            'User API token. User must have access to task, all comments will be attributed to this user.',
          in: 'query',
          name: 'api_key',
          required: false,
          type: ['string']
        },
        {
          description:
            'Comma-separated list of branches which will be automatically inspected. Leave blank to include all branches.',
          in: 'query',
          name: 'restrict_to_branch',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/asana',
      servers: []
    },
    putProjectServicesAssemblas: {
      method: 'PUT',
      operationId: 'putProjectServicesAssemblas',
      parameters: [
        {
          description: 'The authentication token',
          in: 'query',
          name: 'token',
          required: false,
          type: ['string']
        },
        {
          description: 'The subdomain setting',
          in: 'query',
          name: 'subdomain',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/assembla',
      servers: []
    },
    putProjectServicesBamboos: {
      method: 'PUT',
      operationId: 'putProjectServicesBamboos',
      parameters: [
        {
          description: 'Bamboo root URL like ',
          in: 'query',
          name: 'bamboo_url',
          required: false,
          type: ['string']
        },
        {
          description: 'Bamboo build plan key like KEY',
          in: 'query',
          name: 'build_key',
          required: false,
          type: ['string']
        },
        {
          description: 'A user with API access, if applicable',
          in: 'query',
          name: 'username',
          required: false,
          type: ['string']
        },
        {
          description: 'Password of the user',
          in: 'query',
          name: 'password',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/bamboo',
      servers: []
    },
    putProjectServicesBugzillas: {
      method: 'PUT',
      operationId: 'putProjectServicesBugzillas',
      parameters: [
        {
          description: 'New Issue url',
          in: 'query',
          name: 'new_issue_url',
          required: false,
          type: ['string']
        },
        {
          description: 'Issue url',
          in: 'query',
          name: 'issues_url',
          required: false,
          type: ['string']
        },
        {
          description: 'Project url',
          in: 'query',
          name: 'project_url',
          required: false,
          type: ['string']
        },
        {
          description: 'Description',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: 'Title',
          in: 'query',
          name: 'title',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/bugzilla',
      servers: []
    },
    putProjectServicesBuildkites: {
      method: 'PUT',
      operationId: 'putProjectServicesBuildkites',
      parameters: [
        {
          description: 'Buildkite project GitLab token',
          in: 'query',
          name: 'token',
          required: false,
          type: ['string']
        },
        {
          in: 'query',
          name: 'project_url',
          required: false,
          type: ['string']
        },
        {
          description: 'Enable SSL verification',
          in: 'query',
          name: 'enable_ssl_verification',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/services/buildkite',
      servers: []
    },
    putProjectServicesCampfires: {
      method: 'PUT',
      operationId: 'putProjectServicesCampfires',
      parameters: [
        {
          description: 'Campfire token',
          in: 'query',
          name: 'token',
          required: false,
          type: ['string']
        },
        {
          description: 'Campfire subdomain',
          in: 'query',
          name: 'subdomain',
          required: false,
          type: ['string']
        },
        {
          description: 'Campfire room',
          in: 'query',
          name: 'room',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/campfire',
      servers: []
    },
    putProjectServicesCustomIssueTrackers: {
      method: 'PUT',
      operationId: 'putProjectServicesCustomIssueTrackers',
      parameters: [
        {
          description: 'New Issue url',
          in: 'query',
          name: 'new_issue_url',
          required: false,
          type: ['string']
        },
        {
          description: 'Issue url',
          in: 'query',
          name: 'issues_url',
          required: false,
          type: ['string']
        },
        {
          description: 'Project url',
          in: 'query',
          name: 'project_url',
          required: false,
          type: ['string']
        },
        {
          description: 'Description',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        },
        {
          description: 'Title',
          in: 'query',
          name: 'title',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/custom-issue-tracker',
      servers: []
    },
    putProjectServicesDroneCis: {
      method: 'PUT',
      operationId: 'putProjectServicesDroneCis',
      parameters: [
        {
          description: 'Drone CI project specific token',
          in: 'query',
          name: 'token',
          required: false,
          type: ['string']
        },
        {
          in: 'query',
          name: 'drone_url',
          required: false,
          type: ['string']
        },
        {
          description: 'Enable SSL verification',
          in: 'query',
          name: 'enable_ssl_verification',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/services/drone-ci',
      servers: []
    },
    putProjectServicesEmailsOnPushes: {
      method: 'PUT',
      operationId: 'putProjectServicesEmailsOnPushes',
      parameters: [
        {
          description: 'Emails separated by whitespace',
          in: 'query',
          name: 'recipients',
          required: false,
          type: ['string']
        },
        {
          description: 'Disable code diffs',
          in: 'query',
          name: 'disable_diffs',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Send from committer',
          in: 'query',
          name: 'send_from_committer_email',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/services/emails-on-push',
      servers: []
    },
    putProjectServicesExternalWikis: {
      method: 'PUT',
      operationId: 'putProjectServicesExternalWikis',
      parameters: [
        {
          description: 'The URL of the external Wiki',
          in: 'query',
          name: 'external_wiki_url',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/external-wiki',
      servers: []
    },
    putProjectServicesFlowdocks: {
      method: 'PUT',
      operationId: 'putProjectServicesFlowdocks',
      parameters: [
        {
          description: 'Flowdock Git source token',
          in: 'query',
          name: 'token',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/flowdock',
      servers: []
    },
    putProjectServicesGemnasiums: {
      method: 'PUT',
      operationId: 'putProjectServicesGemnasiums',
      parameters: [
        {
          description: 'Your personal API KEY on gemnasium.com',
          in: 'query',
          name: 'api_key',
          required: false,
          type: ['string']
        },
        {
          description: "The project's slug on gemnasium.com",
          in: 'query',
          name: 'token',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/gemnasium',
      servers: []
    },
    putProjectServicesHangoutsChats: {
      method: 'PUT',
      operationId: 'putProjectServicesHangoutsChats',
      parameters: [
        {
          description: 'The Hangouts Chat webhook. e.g. ',
          in: 'query',
          name: 'webhook',
          required: false,
          type: ['string']
        },
        {
          description: 'Send notifications for broken pipelines',
          in: 'query',
          name: 'notify_only_broken_pipelines',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Send notifications only for the default branch',
          in: 'query',
          name: 'notify_only_default_branch',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for push events',
          in: 'query',
          name: 'push_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for issue events',
          in: 'query',
          name: 'issues_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for confidential issue events',
          in: 'query',
          name: 'confidential_issues_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for merge request events',
          in: 'query',
          name: 'merge_requests_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for tag push events',
          in: 'query',
          name: 'tag_push_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for note events',
          in: 'query',
          name: 'note_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for pipeline events',
          in: 'query',
          name: 'pipeline_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for wiki page events',
          in: 'query',
          name: 'wiki_page_events',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/services/hangouts_chat',
      servers: []
    },
    putProjectServicesHipchats: {
      method: 'PUT',
      operationId: 'putProjectServicesHipchats',
      parameters: [
        {
          description: 'Room token',
          in: 'query',
          name: 'token',
          required: false,
          type: ['string']
        },
        {
          description: 'The room color',
          in: 'query',
          name: 'color',
          required: false,
          type: ['string']
        },
        {
          description: 'Enable notifications',
          in: 'query',
          name: 'notify',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Room name or ID',
          in: 'query',
          name: 'room',
          required: false,
          type: ['string']
        },
        {
          description: 'Leave blank for default (v2)',
          in: 'query',
          name: 'api_version',
          required: false,
          type: ['string']
        },
        {
          description: 'Leave blank for default. ',
          in: 'query',
          name: 'server',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/hipchat',
      servers: []
    },
    putProjectServicesIrkers: {
      method: 'PUT',
      operationId: 'putProjectServicesIrkers',
      parameters: [
        {
          description: 'Recipients/channels separated by whitespaces',
          in: 'query',
          name: 'recipients',
          required: false,
          type: ['string']
        },
        {
          description: 'irc://irc.network.net:6697/',
          in: 'query',
          name: 'default_irc_uri',
          required: false,
          type: ['string']
        },
        {
          description: 'localhost',
          in: 'query',
          name: 'server_host',
          required: false,
          type: ['string']
        },
        {
          description: '6659',
          in: 'query',
          name: 'server_port',
          required: false,
          type: ['integer']
        },
        {
          description: 'Colorize messages',
          in: 'query',
          name: 'colorize_messages',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/services/irker',
      servers: []
    },
    putProjectServicesJiras: {
      method: 'PUT',
      operationId: 'putProjectServicesJiras',
      parameters: [
        {
          description:
            'The URL to the JIRA project which is being linked to this GitLab project, e.g., ',
          in: 'path',
          name: 'url',
          required: true,
          type: ['string']
        },
        {
          description:
            'The short identifier for your JIRA project, all uppercase, e.g., ',
          in: 'path',
          name: 'project_key',
          required: true,
          type: ['string']
        },
        {
          description:
            'The username of the user created to be used with GitLab/JIRA.',
          in: 'path',
          name: 'username',
          required: true,
          type: ['string']
        },
        {
          description:
            'The password of the user created to be used with GitLab/JIRA.',
          in: 'path',
          name: 'password',
          required: true,
          type: ['string']
        },
        {
          description:
            'The ID of a transition that moves issues to a closed state. You can find this number under the JIRA workflow administration (',
          in: 'query',
          name: 'jira_issue_transition_id',
          required: false,
          type: ['integer']
        }
      ],
      path: '/projects/:id/services/jira',
      servers: []
    },
    putProjectServicesMattermostSlashCommands: {
      method: 'PUT',
      operationId: 'putProjectServicesMattermostSlashCommands',
      parameters: [
        {
          description: 'The Mattermost token',
          in: 'path',
          name: 'token',
          required: true,
          type: ['string']
        },
        {
          description: 'The username to use to post the message',
          in: 'query',
          name: 'username',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/mattermost-slash-commands',
      servers: []
    },
    putProjectServicesMattermosts: {
      method: 'PUT',
      operationId: 'putProjectServicesMattermosts',
      parameters: [
        {
          description: 'The Mattermost webhook. e.g. ',
          in: 'query',
          name: 'webhook',
          required: false,
          type: ['string']
        },
        {
          description: 'username',
          in: 'query',
          name: 'username',
          required: false,
          type: ['string']
        },
        {
          description: 'Default channel to use if others are not configured',
          in: 'query',
          name: 'channel',
          required: false,
          type: ['string']
        },
        {
          description: 'Send notifications for broken pipelines',
          in: 'query',
          name: 'notify_only_broken_pipelines',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Send notifications only for the default branch',
          in: 'query',
          name: 'notify_only_default_branch',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for push events',
          in: 'query',
          name: 'push_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for issue events',
          in: 'query',
          name: 'issues_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for confidential issue events',
          in: 'query',
          name: 'confidential_issues_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for merge request events',
          in: 'query',
          name: 'merge_requests_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for tag push events',
          in: 'query',
          name: 'tag_push_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for note events',
          in: 'query',
          name: 'note_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for pipeline events',
          in: 'query',
          name: 'pipeline_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for wiki page events',
          in: 'query',
          name: 'wiki_page_events',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'The name of the channel to receive push events notifications',
          in: 'query',
          name: 'push_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive issues events notifications',
          in: 'query',
          name: 'issue_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive confidential issues events notifications',
          in: 'query',
          name: 'confidential_issue_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive merge request events notifications',
          in: 'query',
          name: 'merge_request_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive note events notifications',
          in: 'query',
          name: 'note_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive tag push events notifications',
          in: 'query',
          name: 'tag_push_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive pipeline events notifications',
          in: 'query',
          name: 'pipeline_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive wiki page events notifications',
          in: 'query',
          name: 'wiki_page_channel',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/mattermost',
      servers: []
    },
    putProjectServicesMicrosoftTeams: {
      method: 'PUT',
      operationId: 'putProjectServicesMicrosoftTeams',
      parameters: [
        {
          description: 'The Microsoft Teams webhook. e.g. ',
          in: 'query',
          name: 'webhook',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/microsoft-teams',
      servers: []
    },
    putProjectServicesMockCis: {
      method: 'PUT',
      operationId: 'putProjectServicesMockCis',
      parameters: [
        {
          in: 'query',
          name: 'mock_service_url',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/mock-ci',
      servers: []
    },
    putProjectServicesPipelinesEmails: {
      method: 'PUT',
      operationId: 'putProjectServicesPipelinesEmails',
      parameters: [
        {
          description: 'Comma-separated list of recipient email addresses',
          in: 'path',
          name: 'recipients',
          required: true,
          type: ['string']
        },
        {
          description: 'Add pusher to recipients list',
          in: 'query',
          name: 'add_pusher',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Notify only broken pipelines',
          in: 'query',
          name: 'notify_only_broken_pipelines',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/services/pipelines-email',
      servers: []
    },
    putProjectServicesPivotaltrackers: {
      method: 'PUT',
      operationId: 'putProjectServicesPivotaltrackers',
      parameters: [
        {
          description: 'The PivotalTracker token',
          in: 'query',
          name: 'token',
          required: false,
          type: ['string']
        },
        {
          description:
            'Comma-separated list of branches which will be automatically inspected. Leave blank to include all branches.',
          in: 'query',
          name: 'restrict_to_branch',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/services/pivotaltracker',
      servers: []
    },
    putProjectServicesPrometheuses: {
      method: 'PUT',
      operationId: 'putProjectServicesPrometheuses',
      parameters: [
        {
          description: 'Prometheus API Base URL, like ',
          in: 'query',
          name: 'api_url',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/prometheus',
      servers: []
    },
    putProjectServicesPushovers: {
      method: 'PUT',
      operationId: 'putProjectServicesPushovers',
      parameters: [
        {
          description: 'Your application key',
          in: 'query',
          name: 'api_key',
          required: false,
          type: ['string']
        },
        {
          description: 'Your user key',
          in: 'query',
          name: 'user_key',
          required: false,
          type: ['string']
        },
        {
          description: 'The priority',
          in: 'query',
          name: 'priority',
          required: false,
          type: ['string']
        },
        {
          description: 'Leave blank for all active devices',
          in: 'query',
          name: 'device',
          required: false,
          type: ['string']
        },
        {
          description: 'The sound of the notification',
          in: 'query',
          name: 'sound',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/pushover',
      servers: []
    },
    putProjectServicesRedmines: {
      method: 'PUT',
      operationId: 'putProjectServicesRedmines',
      parameters: [
        {
          description: 'New Issue url',
          in: 'query',
          name: 'new_issue_url',
          required: false,
          type: ['string']
        },
        {
          description: 'Project url',
          in: 'query',
          name: 'project_url',
          required: false,
          type: ['string']
        },
        {
          description: 'Issue url',
          in: 'query',
          name: 'issues_url',
          required: false,
          type: ['string']
        },
        {
          description: 'Description',
          in: 'query',
          name: 'description',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/redmine',
      servers: []
    },
    putProjectServicesSlackSlashCommands: {
      method: 'PUT',
      operationId: 'putProjectServicesSlackSlashCommands',
      parameters: [
        {
          description: 'The Slack token',
          in: 'path',
          name: 'token',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/slack-slash-commands',
      servers: []
    },
    putProjectServicesSlacks: {
      method: 'PUT',
      operationId: 'putProjectServicesSlacks',
      parameters: [
        {
          in: 'query',
          name: 'webhook',
          required: false,
          type: ['string']
        },
        {
          description: 'username',
          in: 'query',
          name: 'username',
          required: false,
          type: ['string']
        },
        {
          description: 'Default channel to use if others are not configured',
          in: 'query',
          name: 'channel',
          required: false,
          type: ['string']
        },
        {
          description: 'Send notifications for broken pipelines',
          in: 'query',
          name: 'notify_only_broken_pipelines',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Send notifications only for the default branch',
          in: 'query',
          name: 'notify_only_default_branch',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for push events',
          in: 'query',
          name: 'push_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for issue events',
          in: 'query',
          name: 'issues_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for confidential issue events',
          in: 'query',
          name: 'confidential_issues_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for merge request events',
          in: 'query',
          name: 'merge_requests_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for tag push events',
          in: 'query',
          name: 'tag_push_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for note events',
          in: 'query',
          name: 'note_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for pipeline events',
          in: 'query',
          name: 'pipeline_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Enable notifications for wiki page events',
          in: 'query',
          name: 'wiki_page_events',
          required: false,
          type: ['boolean']
        },
        {
          description:
            'The name of the channel to receive push events notifications',
          in: 'query',
          name: 'push_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive issues events notifications',
          in: 'query',
          name: 'issue_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive confidential issues events notifications',
          in: 'query',
          name: 'confidential_issue_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive merge request events notifications',
          in: 'query',
          name: 'merge_request_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive note events notifications',
          in: 'query',
          name: 'note_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive tag push events notifications',
          in: 'query',
          name: 'tag_push_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive pipeline events notifications',
          in: 'query',
          name: 'pipeline_channel',
          required: false,
          type: ['string']
        },
        {
          description:
            'The name of the channel to receive wiki page events notifications',
          in: 'query',
          name: 'wiki_page_channel',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/slack',
      servers: []
    },
    putProjectServicesTeamcities: {
      method: 'PUT',
      operationId: 'putProjectServicesTeamcities',
      parameters: [
        {
          description: 'TeamCity root URL like ',
          in: 'query',
          name: 'teamcity_url',
          required: false,
          type: ['string']
        },
        {
          description: 'Build configuration ID',
          in: 'query',
          name: 'build_type',
          required: false,
          type: ['string']
        },
        {
          description: 'A user with permissions to trigger a manual build',
          in: 'query',
          name: 'username',
          required: false,
          type: ['string']
        },
        {
          description: 'The password of the user',
          in: 'query',
          name: 'password',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/services/teamcity',
      servers: []
    }
  },
  sidekiqMetrics: {},
  snippets: {
    deleteSnippetById: {
      method: 'DELETE',
      operationId: 'deleteSnippetById',
      parameters: [
        {
          description: 'The ID of a snippet',
          in: 'path',
          name: 'id',
          required: true,
          type: ['Integer']
        }
      ],
      path: '/snippets/:id',
      servers: []
    },
    getSnippetById: {
      method: 'GET',
      operationId: 'getSnippetById',
      parameters: [
        {
          description: 'The ID of a snippet',
          in: 'path',
          name: 'id',
          required: true,
          type: ['Integer']
        }
      ],
      path: '/snippets/:id',
      servers: []
    },
    getSnippetUserAgentDetails: {
      method: 'GET',
      operationId: 'getSnippetUserAgentDetails',
      parameters: [
        {
          description: 'The ID of a snippet',
          in: 'path',
          name: 'id',
          required: true,
          type: ['Integer']
        }
      ],
      path: '/snippets/:id/user_agent_detail',
      servers: []
    },
    getSnippetsPublics: {
      method: 'GET',
      operationId: 'getSnippetsPublics',
      parameters: [
        {
          description: 'number of snippets to return per page',
          in: 'query',
          name: 'per_page',
          required: false,
          type: ['Integer']
        },
        {
          description: 'the page to retrieve',
          in: 'query',
          name: 'page',
          required: false,
          type: ['Integer']
        }
      ],
      path: '/snippets/public',
      servers: []
    },
    postSnippets: {
      method: 'POST',
      operationId: 'postSnippets',
      parameters: [
        {
          description: 'The title of a snippet',
          in: 'path',
          name: 'title',
          required: true,
          type: ['String']
        },
        {
          description: 'The name of a snippet file',
          in: 'path',
          name: 'file_name',
          required: true,
          type: ['String']
        },
        {
          description: 'The content of a snippet',
          in: 'path',
          name: 'content',
          required: true,
          type: ['String']
        },
        {
          description: 'The description of a snippet',
          in: 'query',
          name: 'description',
          required: false,
          type: ['String']
        },
        {
          description: "The snippet's visibility",
          in: 'query',
          name: 'visibility',
          required: false,
          type: ['String']
        }
      ],
      path: '/snippets',
      servers: []
    },
    putSnippetById: {
      method: 'PUT',
      operationId: 'putSnippetById',
      parameters: [
        {
          description: 'The ID of a snippet',
          in: 'path',
          name: 'id',
          required: true,
          type: ['Integer']
        },
        {
          description: 'The title of a snippet',
          in: 'query',
          name: 'title',
          required: false,
          type: ['String']
        },
        {
          description: 'The name of a snippet file',
          in: 'query',
          name: 'file_name',
          required: false,
          type: ['String']
        },
        {
          description: 'The description of a snippet',
          in: 'query',
          name: 'description',
          required: false,
          type: ['String']
        },
        {
          description: 'The content of a snippet',
          in: 'query',
          name: 'content',
          required: false,
          type: ['String']
        },
        {
          description: "The snippet's visibility",
          in: 'query',
          name: 'visibility',
          required: false,
          type: ['String']
        }
      ],
      path: '/snippets/:id',
      servers: []
    }
  },
  systemHooks: {
    deleteHookById: {
      method: 'DELETE',
      operationId: 'deleteHookById',
      parameters: [
        {
          description: 'The ID of the hook',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/hooks/:id',
      servers: []
    },
    getHookById: {
      method: 'GET',
      operationId: 'getHookById',
      parameters: [
        {
          description: 'The ID of the hook',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/hooks/:id',
      servers: []
    },
    postHooks: {
      method: 'POST',
      operationId: 'postHooks',
      parameters: [
        {
          description: 'The hook URL',
          in: 'path',
          name: 'url',
          required: true,
          type: ['string']
        },
        {
          description:
            'Secret token to validate received payloads; this will not be returned in the response',
          in: 'query',
          name: 'token',
          required: false,
          type: ['string']
        },
        {
          description: 'When true, the hook will fire on push events',
          in: 'query',
          name: 'push_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'When true, the hook will fire on new tags being pushed',
          in: 'query',
          name: 'tag_push_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Trigger hook on merge requests events',
          in: 'query',
          name: 'merge_requests_events',
          required: false,
          type: ['boolean']
        },
        {
          description: 'Do SSL verification when triggering the hook',
          in: 'query',
          name: 'enable_ssl_verification',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/hooks',
      servers: []
    }
  },
  tags: {
    getProjectRepositoryTagByTagName: {
      method: 'GET',
      operationId: 'getProjectRepositoryTagByTagName',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The name of the tag',
          in: 'path',
          name: 'tag_name',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/tags/:tag_name',
      servers: []
    },
    getProjectRepositoryTags: {
      method: 'GET',
      operationId: 'getProjectRepositoryTags',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'Return tags ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return tags sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/repository/tags',
      servers: []
    }
  },
  templatesgitignores: {
    getTemplatesGitignoreByKey: {
      method: 'GET',
      operationId: 'getTemplatesGitignoreByKey',
      parameters: [
        {
          description: 'The key of the gitignore template',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        }
      ],
      path: '/templates/gitignores/:key',
      servers: []
    }
  },
  templatesgitlabCiYmls: {
    getTemplatesGitlabCiYmlByKey: {
      method: 'GET',
      operationId: 'getTemplatesGitlabCiYmlByKey',
      parameters: [
        {
          description: 'The key of the GitLab CI YML template',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        }
      ],
      path: '/templates/gitlab_ci_ymls/:key',
      servers: []
    }
  },
  templateslicenses: {
    getTemplatesLicenseByKey: {
      method: 'GET',
      operationId: 'getTemplatesLicenseByKey',
      parameters: [
        {
          description: 'The key of the license template',
          in: 'path',
          name: 'key',
          required: true,
          type: ['string']
        },
        {
          description: 'The copyrighted project name',
          in: 'query',
          name: 'project',
          required: false,
          type: ['string']
        },
        {
          description: 'The full-name of the copyright holder',
          in: 'query',
          name: 'fullname',
          required: false,
          type: ['string']
        }
      ],
      path: '/templates/licenses/:key',
      servers: []
    },
    getTemplatesLicenses: {
      method: 'GET',
      operationId: 'getTemplatesLicenses',
      parameters: [
        {
          description: 'If passed, returns only popular licenses',
          in: 'query',
          name: 'popular',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/templates/licenses',
      servers: []
    }
  },
  todos: {
    getTodos: {
      method: 'GET',
      operationId: 'getTodos',
      parameters: [
        {
          description: 'The action to be filtered. Can be ',
          in: 'query',
          name: 'action',
          required: false,
          type: ['string']
        },
        {
          description: 'The ID of an author',
          in: 'query',
          name: 'author_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'The ID of a project',
          in: 'query',
          name: 'project_id',
          required: false,
          type: ['integer']
        },
        {
          description: 'The state of the todo. Can be either ',
          in: 'query',
          name: 'state',
          required: false,
          type: ['string']
        },
        {
          description: 'The type of a todo. Can be either ',
          in: 'query',
          name: 'type',
          required: false,
          type: ['string']
        }
      ],
      path: '/todos',
      servers: []
    },
    postTodoMarkAsDones: {
      method: 'POST',
      operationId: 'postTodoMarkAsDones',
      parameters: [
        {
          description: 'The ID of a todo',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/todos/:id/mark_as_done',
      servers: []
    }
  },
  users: {
    curlRequestByRequest: {
      method: 'curl',
      operationId: 'curlRequestByRequest',
      parameters: [
        {
          description: 'The ID of the user',
          in: 'path',
          name: 'user_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of the impersonation token',
          in: 'path',
          name: 'impersonation_token_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '--request',
      servers: []
    },
    deleteUserGpgKeyByKeyId: {
      method: 'DELETE',
      operationId: 'deleteUserGpgKeyByKeyId',
      parameters: [
        {
          description: 'The ID of the user',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of the GPG key',
          in: 'path',
          name: 'key_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/users/:id/gpg_keys/:key_id',
      servers: []
    },
    getUserActivities: {
      method: 'GET',
      operationId: 'getUserActivities',
      parameters: [
        {
          description: 'Date string in the format YEAR-MONTH-DAY, e.g. ',
          in: 'query',
          name: 'from',
          required: false,
          type: ['string']
        }
      ],
      path: '/user/activities',
      servers: []
    },
    getUserGpgKeyByKeyId: {
      method: 'GET',
      operationId: 'getUserGpgKeyByKeyId',
      parameters: [
        {
          description: 'The ID of the user',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of the GPG key',
          in: 'path',
          name: 'key_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/users/:id/gpg_keys/:key_id',
      servers: []
    },
    getUserGpgKeys: {
      method: 'GET',
      operationId: 'getUserGpgKeys',
      parameters: [
        {
          description: 'The ID of the user',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/users/:id/gpg_keys',
      servers: []
    },
    getUserImpersonationTokenByImpersonationTokenId: {
      method: 'GET',
      operationId: 'getUserImpersonationTokenByImpersonationTokenId',
      parameters: [
        {
          description: 'The ID of the user',
          in: 'path',
          name: 'user_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of the impersonation token',
          in: 'path',
          name: 'impersonation_token_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/users/:user_id/impersonation_tokens/:impersonation_token_id',
      servers: []
    },
    getUserImpersonationTokens: {
      method: 'GET',
      operationId: 'getUserImpersonationTokens',
      parameters: [
        {
          description: 'The ID of the user',
          in: 'path',
          name: 'user_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'filter tokens based on state (',
          in: 'query',
          name: 'state',
          required: false,
          type: ['string']
        }
      ],
      path: '/users/:user_id/impersonation_tokens',
      servers: []
    },
    getUsers: {
      method: 'GET',
      operationId: 'getUsers',
      parameters: [
        {
          description: 'Return projects ordered by ',
          in: 'query',
          name: 'order_by',
          required: false,
          type: ['string']
        },
        {
          description: 'Return projects sorted in ',
          in: 'query',
          name: 'sort',
          required: false,
          type: ['string']
        },
        {
          description:
            'Filter users by Two-factor authentication. Filter values are ',
          in: 'query',
          name: 'two_factor',
          required: false,
          type: ['string']
        }
      ],
      path: '/users',
      servers: []
    },
    postUserGpgKeys: {
      method: 'POST',
      operationId: 'postUserGpgKeys',
      parameters: [
        {
          description: 'The ID of the user',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The ID of the GPG key',
          in: 'path',
          name: 'key_id',
          required: true,
          type: ['integer']
        }
      ],
      path: '/users/:id/gpg_keys',
      servers: []
    },
    postUserImpersonationTokens: {
      method: 'POST',
      operationId: 'postUserImpersonationTokens',
      parameters: [
        {
          description: 'The ID of the user',
          in: 'path',
          name: 'user_id',
          required: true,
          type: ['integer']
        },
        {
          description: 'The name of the impersonation token',
          in: 'path',
          name: 'name',
          required: true,
          type: ['string']
        },
        {
          description:
            'The expiration date of the impersonation token in ISO format (',
          in: 'query',
          name: 'expires_at',
          required: false,
          type: ['date']
        },
        {
          description: 'The array of scopes of the impersonation token (',
          in: 'path',
          name: 'scopes',
          required: true,
          type: ['array']
        }
      ],
      path: '/users/:user_id/impersonation_tokens',
      servers: []
    }
  },
  v3ToV4: {},
  version: {},
  wikis: {
    deleteProjectWikiBySlug: {
      method: 'DELETE',
      operationId: 'deleteProjectWikiBySlug',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The slug (a unique string) of the wiki page',
          in: 'path',
          name: 'slug',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/wikis/:slug',
      servers: []
    },
    getProjectWikiBySlug: {
      method: 'GET',
      operationId: 'getProjectWikiBySlug',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The slug (a unique string) of the wiki page',
          in: 'path',
          name: 'slug',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/wikis/:slug',
      servers: []
    },
    getProjectWikis: {
      method: 'GET',
      operationId: 'getProjectWikis',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: "Include pages' content",
          in: 'query',
          name: 'with_content',
          required: false,
          type: ['boolean']
        }
      ],
      path: '/projects/:id/wikis',
      servers: []
    },
    postProjectWikis: {
      method: 'POST',
      operationId: 'postProjectWikis',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The content of the wiki page',
          in: 'path',
          name: 'content',
          required: true,
          type: ['string']
        },
        {
          description: 'The title of the wiki page',
          in: 'path',
          name: 'title',
          required: true,
          type: ['string']
        },
        {
          description: 'The format of the wiki page. Available formats are: ',
          in: 'query',
          name: 'format',
          required: false,
          type: ['string']
        }
      ],
      path: '/projects/:id/wikis',
      servers: []
    },
    putProjectWikiBySlug: {
      method: 'PUT',
      operationId: 'putProjectWikiBySlug',
      parameters: [
        {
          description: 'The ID or ',
          in: 'path',
          name: 'id',
          required: true,
          type: ['integer', 'string']
        },
        {
          description: 'The content of the wiki page',
          in: 'query',
          name: 'content',
          required: false,
          type: ['string']
        },
        {
          description: 'The title of the wiki page',
          in: 'query',
          name: 'title',
          required: false,
          type: ['string']
        },
        {
          description: 'The format of the wiki page. Available formats are: ',
          in: 'query',
          name: 'format',
          required: false,
          type: ['string']
        },
        {
          description: 'The slug (a unique string) of the wiki page',
          in: 'path',
          name: 'slug',
          required: true,
          type: ['string']
        }
      ],
      path: '/projects/:id/wikis/:slug',
      servers: []
    }
  }
}

export default gitlab
