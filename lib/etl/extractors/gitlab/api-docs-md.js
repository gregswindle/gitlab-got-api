import got from 'got'
import remark from 'remark'

const md = {
  baseUrl: 'https://gitlab.com/gitlab-org/gitlab-ee/raw/master/doc/api',
  options: {
    headers: {
      json: false,
      'private-token': process.env.GITLAB_TOKEN
    }
  },
  paths: [
    '/award_emoji.md',
    '/branches.md',
    '/broadcast_messages.md',
    '/project_level_variables.md',
    '/group_level_variables.md',
    '/snippets.md',
    '/commits.md',
    '/custom_attributes.md',
    '/deployments.md',
    '/deploy_keys.md',
    '/environments.md',
    '/epics.md',
    '/epic_issues.md',
    '/events.md',
    '/features.md',
    '/geo_nodes.md',
    '/templates/gitignores.md',
    '/templates/gitlab_ci_ymls.md',
    '/groups.md',
    '/access_requests.md',
    '/group_badges.md',
    '/members.md',
    '/issues.md',
    '/boards.md',
    '/issue_links.md',
    '/group_boards.md',
    '/jobs.md',
    '/keys.md',
    '/labels.md',
    '/license.md',
    '/markdown.md',
    '/merge_requests.md',
    '/merge_request_approvals.md',
    '/milestones.md',
    '/group_milestones.md',
    '/namespaces.md',
    '/notes.md',
    '/discussions.md',
    '/notification_settings.md',
    '/templates/licenses.md',
    '/pages_domains.md',
    '/pipelines.md',
    '/pipeline_triggers.md',
    '/pipeline_schedules.md',
    '/projects.md',
    '/access_requests.md',
    '/project_badges.md',
    '/project_import_export.md',
    '/members.md',
    '/project_snippets.md',
    '/protected_branches.md',
    '/repositories.md',
    '/repository_files.md',
    '/runners.md',
    '/search.md',
    '/services.md',
    '/settings.md',
    '/sidekiq_metrics.md',
    '/system_hooks.md',
    '/tags.md',
    '/todos.md',
    '/users.md',
    '/lint.md',
    '/v3_to_v4.md',
    '/version.md',
    '/wikis.md'
  ],

  externalDocs (paths = md.paths) {
    return paths.map(path => {
      return {
        description: null,
        url: `${this.baseUrl}${path}`
      }
    })
  },

  apiDocs: {
    externalDocs (paths) {
      return md.externalDocs(paths)
    },
    mdast: {
      async extract (url, options = md.options) {
        let resource = null
        try {
          const {body, headers} = await got(url, options)
          resource = {
            ast: remark.parse(body),
            body,
            headers,
            url
          }
        } catch (err) {
          resource = err
        }
        return resource
      },

      async extractAll (paths, options = md.options) {
        return md.paths.map(async path => {
          const url = md.baseUrl + path
          const ast = await md.apiDocs.extract(url, options)
          return ast
        })
      }
    }
  }
}

export default md
